#pragma once

#include "Object3d.h"
#include "SafeDelete.h"

using namespace DirectX;

class BossStageEnemy
{
public: // メンバ関数
	// コンストラクタ
	BossStageEnemy();

	// デストラクタ
	~BossStageEnemy();

	// 初期化
	void Initialize();

	// 更新
	void Update();

	// 描画
	void Draw();

	// ゲッター
	void Getter();

	// セッター
	void Setter();

	// アクション
	void Action();

	// エフェクト
	void Effect();

	// リセット
	void Reset();

	// ダメージカウントの取得
	unsigned int GetDamageCount() { return damageCount; }

	// トランスフォームフラグの取得
	unsigned int GetTransformFlag() { return transformFlag; }

	// デスフラグの取得
	bool GetDeathFlag() { return deathFlag; }

	// エフェクトのY座標の取得
	float GetEffectPositionY() { return EffectPosition[0].y; }

	// エフェクトの範囲の取得
	float GetEffectRange() { return effectRange; }

	// 座標の取得
	XMFLOAT3 GetPosition() { return objEnemy[0]->GetPosition(); }

	// スピードの設定
	void SetSpeed1() { speed = 0.4f; }

	// ジャンプパワーの設定
	void SetJumpPower() { jumpPower = 0.35f; }

	// ジャンプの高さの設定
	void SetJumpHeight1() { jumpHeight = -0.35f; }
	void SetJumpHeight2() { jumpHeight = -0.7f; }

	// トランスフォームフラグの設定
	void SetTransformFlag1() { transformFlag = 1; }
	void SetTransformFlag2() { transformFlag = 2; }
	void SetTransformFlag3() { transformFlag = 3; }

	// ダメージカウントの設定
	void SetDamageCount() { damageCount = 120; }

	// フリーズフラグの設定(True)
	void SetFreezeFlagTrue() { freezeFlag = 1; }

	// フリーズフラグの設定(False)
	void SetFreezeFlagFalse() { freezeFlag = 0; }

	// デスフラグの設定
	void SetDeathFlag() { deathFlag = 1; }

	// エフェクトの設定
	void SetEffect();

private: // メンバ変数
	float speed = 0.2f;
	float jumpPower = 0;
	float jumpHeight = 0.0f;
	unsigned int damageCount = 0;
	unsigned int transformFlag = 0;
	bool freezeFlag = 0;
	bool landFlag = 0;
	bool moveFlag[3] = {};
	bool deathFlag = 0;

	XMFLOAT3 EnemyPosition = { 42,1,0 };
	XMFLOAT3 WingPosition[4] = { { 44,1,-2 },{ 40,1,-2 }, { 44,1,2 }, { 40,1,2 } };
	XMFLOAT3 EnemyRotation = { 0,0,0 };
	XMFLOAT3 WingRotation = { 0,0,0 };

	XMFLOAT3 EffectPosition[8] = {};
	float effectRange = 0.0f;

	Model* modelBrownEnemy = nullptr;
	Model* modelGrayEnemy = nullptr;
	Model* modelDrone = nullptr;
	Model* modelWing = nullptr;
	Model* modelArrow = nullptr;
	Model* modelYellowBall = nullptr;

	Object3d* objEnemy[2] = {};
	Object3d* objDrone = nullptr;
	Object3d* objWing[4] = {};
	Object3d* objArrow[2] = {};
	Object3d* objEffect[8] = {};
};