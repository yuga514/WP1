#include "BossStageEnemy.h"

// コンストラクタ
BossStageEnemy::BossStageEnemy()
{
}

// デストラクタ
BossStageEnemy::~BossStageEnemy()
{
	safe_delete(modelBrownEnemy);
	safe_delete(modelGrayEnemy);
	safe_delete(modelDrone);
	safe_delete(modelWing);
	safe_delete(modelArrow);
	safe_delete(modelYellowBall);

	for (int i = 0; i < 2; i++) {
		safe_delete(objEnemy[i]);
	}
	safe_delete(objDrone);
	for (int i = 0; i < 4; i++) {
		safe_delete(objWing[i]);
	}
	for (int i = 0; i < 2; i++) {
		safe_delete(objArrow[i]);
	}
	for (int i = 0; i < 8; i++) {
		safe_delete(objEffect[i]);
	}
}

// 初期化
void BossStageEnemy::Initialize()
{
	// モデル読み込み
	modelBrownEnemy = Model::CreateFromOBJ("brownEnemy");
	modelGrayEnemy = Model::CreateFromOBJ("grayEnemy");
	modelDrone = Model::CreateFromOBJ("drone");
	modelWing = Model::CreateFromOBJ("wing");
	modelArrow = Model::CreateFromOBJ("arrow");
	modelYellowBall = Model::CreateFromOBJ("yellowBall");

	// 3Dオブジェクト生成
	objEnemy[0] = Object3d::Create(modelBrownEnemy);
	objEnemy[1] = Object3d::Create(modelGrayEnemy);
	objDrone = Object3d::Create(modelDrone);
	for (int i = 0; i < 4; i++) {
		objWing[i] = Object3d::Create(modelWing);
	}
	for (int i = 0; i < 2; i++) {
		objArrow[i] = Object3d::Create(modelArrow);
	}
	for (int i = 0; i < 8; i++) {
		objEffect[i] = Object3d::Create(modelYellowBall);
	}

	// エフェクト配置
	for (int i = 0; i < 8; i++) {
		EffectPosition[i] = { -10, -10, 0 };
	}
}

// 更新
void BossStageEnemy::Update()
{
	// ゲッター
	Getter();

	// アクション
	if (freezeFlag == 0 && deathFlag == 0) { Action(); }
	if (deathFlag == 1 && 1 < damageCount) {
		EnemyPosition.y += 0.05f;
		for (int i = 0; i < 4; i++) {
			WingPosition[i].y += 0.05f;
		}
	}

	// ダメージを受けたら
	if (0 < damageCount) { damageCount--; }

	// エフェクト
	Effect();

	// セッター
	Setter();

	// アップデート
	for (int i = 0; i < 2; i++) {
		objEnemy[i]->Update();
	}
	objDrone->Update();
	for (int i = 0; i < 4; i++) {
		objWing[i]->Update();
	}
	for (int i = 0; i < 2; i++) {
		objArrow[i]->Update();
	}
	for (int i = 0; i < 8; i++) {
		objEffect[i]->Update();
	}
}

// 描画
void BossStageEnemy::Draw()
{
	if ((deathFlag == 0 || deathFlag == 1 && damageCount != 0) && damageCount % 2 == 0) {
		if (transformFlag == 0) {
			objEnemy[0]->Draw();
		}
		if (0 < transformFlag) {
			objEnemy[1]->Draw();
		}
		if (transformFlag == 4) {
			objDrone->Draw();
			for (int i = 0; i < 4; i++) {
				objWing[i]->Draw();
			}
		}
	}
	if (22 < EnemyPosition.y) {
		objArrow[0]->Draw();
	}
	if (46 < EnemyPosition.x) {
		objArrow[1]->Draw();
	}
	for (int i = 0; i < 8; i++) {
		if (deathFlag == 1 && EffectPosition[0].y < effectRange) {
			objEffect[i]->Draw();
		}
	}
}

// ゲッター
void BossStageEnemy::Getter()
{
	// ゲットポジション
	for (int i = 0; i < 2; i++) {
		objEnemy[i]->GetPosition();
	}
	objDrone->GetPosition();
	for (int i = 0; i < 4; i++) {
		objWing[i]->GetPosition();
	}
	for (int i = 0; i < 2; i++) {
		objArrow[i]->GetPosition();
	}
	for (int i = 0; i < 8; i++) {
		objEffect[i]->GetPosition();
	}

	// ゲットローテーション
	for (int i = 0; i < 2; i++) {
		objEnemy[i]->GetRotation();
	}
	for (int i = 0; i < 4; i++) {
		objWing[i]->GetRotation();
	}
	objArrow[1]->GetRotation();

	// ゲットスケール
	for (int i = 0; i < 2; i++) {
		objEnemy[i]->GetScale();
	}
	for (int i = 0; i < 4; i++) {
		objWing[i]->GetScale();
	}
	for (int i = 0; i < 8; i++) {
		objEffect[i]->GetScale();
	}
}

// セッター
void BossStageEnemy::Setter()
{
	// セットポジション
	for (int i = 0; i < 2; i++) {
		objEnemy[i]->SetPosition(EnemyPosition);
	}
	objDrone->SetPosition(EnemyPosition);
	for (int i = 0; i < 4; i++) {
		objWing[i]->SetPosition(WingPosition[i]);
	}
	objArrow[0]->SetPosition({ EnemyPosition.x, 17, 0 });
	objArrow[1]->SetPosition({ 39, EnemyPosition.y, 0 });
	for (int i = 0; i < 8; i++) {
		objEffect[i]->SetPosition(EffectPosition[i]);
	}

	// セットローテーション
	for (int i = 0; i < 2; i++) {
		objEnemy[i]->SetRotation(EnemyRotation);
	}
	for (int i = 0; i < 4; i++) {
		objWing[i]->SetRotation(WingRotation);
	}
	objArrow[1]->SetRotation({ 0,0,-90 });

	// セットスケール
	for (int i = 0; i < 2; i++) {
		objEnemy[i]->SetScale({ 2,2,2 });
	}
	objDrone->SetScale({ 2,2,2 });
	for (int i = 0; i < 4; i++) {
		objWing[i]->SetScale({ 2,2,2 });
	}
	for (int i = 0; i < 8; i++) {
		objEffect[i]->SetScale({ 2,2,2 });
	}
}

// アクション
void BossStageEnemy::Action()
{
	// 最終形態になるまでの行動
	if (transformFlag < 3) {
		// 着地
		if (EnemyPosition.y == 1) {
			landFlag = 1;
		}
		if (1 < EnemyPosition.y) {
			landFlag = 0;
		}

		// 落下処理
		EnemyPosition.y -= jumpPower;
		for (int i = 0; i < 4; i++) {
			WingPosition[i].y -= jumpPower;
		}

		// 落下加速度を加える
		jumpPower += 0.01f;

		// 地面についていたら止まる
		if (EnemyPosition.y < 1) {
			EnemyPosition.y = 1.0f;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].y = 1.0f;
			}
		}
	}

	// HPが1になるまでの行動
	if (transformFlag < 2) {
		// 左移動
		if (moveFlag[0] == 0) {
			EnemyPosition.x -= speed;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].x -= speed;
			}
		}

		// 右移動
		if (moveFlag[0] == 1) {
			EnemyPosition.x += speed;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].x += speed;
			}
		}

		// ジャンプ
		if ((int)EnemyPosition.x % 2 == 0 && landFlag == 1) {
			jumpPower = jumpHeight;
		}

		// 回転
		EnemyRotation.y += 3.6f;
		if (EnemyRotation.y == 360) {
			EnemyRotation.y = 0.0f;
		}

		// 方向転換
		if (EnemyPosition.x < 0) {
			moveFlag[0] = 1;
		}
		if (42 < EnemyPosition.x) {
			moveFlag[0] = 0;
		}
	}

	// HPが1になったときの行動
	if (transformFlag == 2) {
		if (EnemyPosition.x < 51) {
			EnemyPosition.x += speed;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].x += speed;
			}
		}
		if (51 < EnemyPosition.x && landFlag == 1) {
			transformFlag = 3;
		}
		EnemyRotation.y = 180.0f;
	}

	// 画面外にいったときの行動
	if (transformFlag == 3) {
		if (EnemyPosition.y < 12) {
			EnemyPosition.y += 0.05f;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].y += 0.05f;
			}
		}
		if (10 < EnemyPosition.y) {
			transformFlag = 4;
		}
	}

	// 最終形態になったときの行動
	if (transformFlag == 4) {
		// 左移動
		if (moveFlag[0] == 0) {
			EnemyPosition.x -= speed;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].x -= speed;
			}
		}

		// 右移動
		if (moveFlag[0] == 1) {
			EnemyPosition.x += speed;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].x += speed;
			}
		}

		// 上移動
		if (moveFlag[1] == 0) {
			EnemyPosition.y += speed;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].y += speed;
			}
		}

		// 下移動
		if (moveFlag[1] == 1) {
			EnemyPosition.y -= speed;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].y -= speed;
			}
		}

		// 前移動
		if (moveFlag[2] == 0) {
			EnemyPosition.z += speed;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].z += speed;
			}
		}

		// 後移動
		if (moveFlag[2] == 1) {
			EnemyPosition.z -= speed;
			for (int i = 0; i < 4; i++) {
				WingPosition[i].z -= speed;
			}
		}

		// 回転
		WingRotation.y += 36.0f;
		if (WingRotation.y == 360) {
			WingRotation.y = 0.0f;
		}

		// 方向転換
		if (EnemyPosition.x < 0) {
			moveFlag[0] = 1;
			EnemyRotation.y = 180.0f;
		}
		if (42 < EnemyPosition.x) {
			moveFlag[0] = 0;
			EnemyRotation.y = 0.0f;
		}
		if (19 < EnemyPosition.y) {
			moveFlag[1] = 1;
		}
		if (EnemyPosition.y < 1) {
			moveFlag[1] = 0;
		}
		if (9 < EnemyPosition.z) {
			moveFlag[2] = 1;
			if (moveFlag[0] == 0) {
				EnemyRotation.y = -45.0f;
			}
			else if (moveFlag[0] == 1) {
				EnemyRotation.y = -135.0f;
			}
		}
		if (EnemyPosition.z < -9) {
			moveFlag[2] = 0;
			if (moveFlag[0] == 0) {
				EnemyRotation.y = 45.0f;
			}
			else if (moveFlag[0] == 1) {
				EnemyRotation.y = 135.0f;
			}
		}

		// スピード
		speed = 0.8f;
	}
}

// エフェクト
void BossStageEnemy::Effect()
{
	if (deathFlag == 1 && damageCount == 0 && EffectPosition[0].y < effectRange) {
		EffectPosition[0].y += 0.7f;
		EffectPosition[1].x += 0.7f / 1.41421356f;
		EffectPosition[1].y += 0.7f / 1.41421356f;
		EffectPosition[2].x += 0.7f;
		EffectPosition[3].x += 0.7f / 1.41421356f;
		EffectPosition[3].y -= 0.7f / 1.41421356f;
		EffectPosition[4].y -= 0.7f;
		EffectPosition[5].x -= 0.7f / 1.41421356f;
		EffectPosition[5].y -= 0.7f / 1.41421356f;
		EffectPosition[6].x -= 0.7f;
		EffectPosition[7].x -= 0.7f / 1.41421356f;
		EffectPosition[7].y += 0.7f / 1.41421356f;
	}
	effectRange = EnemyPosition.y + 9.0f;
}

// エフェクトの設定
void BossStageEnemy::SetEffect()
{
	for (int i = 0; i < 8; i++) {
		EffectPosition[i].x = EnemyPosition.x;
		EffectPosition[i].y = EnemyPosition.y;
		EffectPosition[i].z = -3.0f;
	}
}

// リセット
void BossStageEnemy::Reset()
{
	speed = 0.2f;
	jumpPower = 0;
	jumpHeight = 0.0f;
	damageCount = 0;
	transformFlag = 0;
	freezeFlag = 0;
	landFlag = 0;
	for (int i = 0; i < 3; i++) {
		moveFlag[i] = 0;
	}
	deathFlag = 0;
	EnemyPosition = { 42,1,0 };
	WingPosition[0] = { 44,1,-2 };
	WingPosition[1] = { 40,1,-2 };
	WingPosition[2] = { 44,1,2 };
	WingPosition[3] = { 40,1,2 };
	EnemyRotation = { 0,0,0 };
	WingRotation = { 0,0,0 };
	// エフェクト配置
	for (int i = 0; i < 8; i++) {
		EffectPosition[i] = { -10, -10, 0 };
	}
}