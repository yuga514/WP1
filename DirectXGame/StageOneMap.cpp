#include "StageOneMap.h"

// コンストラクタ
StageOneMap::StageOneMap()
{
}

// デストラクタ
StageOneMap::~StageOneMap()
{
	safe_delete(modelBlock);
	safe_delete(modelDoor);
	safe_delete(modelOperation);
	safe_delete(modelBrownBlock);
	safe_delete(modelBrownBall);

	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				safe_delete(objBlock[y][x]);
			}
			if (map[y][x] == DOOR)
			{
				safe_delete(objDoor[y][x]);
			}
			if (map[y][x] == OPERATION)
			{
				safe_delete(objOperation[y][x]);
			}
			if (map[y][x] == BROWN)
			{
				safe_delete(objBrownBlock[y][x]);
			}
		}
	}
	for (int i = 0; i < 24; i++) {
		safe_delete(objEffect[i]);
	}
}

// 初期化
void StageOneMap::Initialize()
{
	// モデル読み込み
	modelBlock = Model::CreateFromOBJ("block");
	modelDoor = Model::CreateFromOBJ("door");
	modelOperation = Model::CreateFromOBJ("operation");
	modelBrownBlock = Model::CreateFromOBJ("brownBlock");
	modelBrownBall = Model::CreateFromOBJ("brownBall");

	// 3Dオブジェクト生成
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x] = Object3d::Create(modelBlock);
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x] = Object3d::Create(modelDoor);
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x] = Object3d::Create(modelOperation);
			}
			if (map[y][x] == BROWN)
			{
				objBrownBlock[y][x] = Object3d::Create(modelBrownBlock);
			}
		}
	}
	for (int i = 0; i < 24; i++) {
		objEffect[i] = Object3d::Create(modelBrownBall);
	}

	// マップチップ配置
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			MapPosition[y][x].x = 3.0f * x - 3.0f;
			MapPosition[y][x].y = -3.0f * y + 19.8f;
		}
	}

	// エフェクト配置
	for (int i = 0; i < 24; i++) {
		if (i < 8) {
			EffectPosition[i] = { 15, 0, -3 };
		}
		if (7 < i && i < 16) {
			EffectPosition[i] = { 66, 0, -3 };
		}
		if (15 < i) {
			EffectPosition[i] = { 84, 0, -3 };
		}
	}
}

// 更新
void StageOneMap::Update()
{
	// ゲッター
	Getter();

	// エフェクト
	Effect();

	// セッター
	Setter();

	// アップデート
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->Update();
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->Update();
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->Update();
			}
			if (map[y][x] == BROWN)
			{
				objBrownBlock[y][x]->Update();
			}
		}
	}
	for (int i = 0; i < 24; i++) {
		objEffect[i]->Update();
	}
}

// 描画
void StageOneMap::Draw()
{
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->Draw();
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->Draw();
			}
			if (map[y][x] == OPERATION)
			{
				if (operationFlag == 1) { objOperation[5][31]->Draw(); }
			}
			if (map[y][x] == BROWN)
			{
				if (blockFlag[0] == 0) { objBrownBlock[7][6]->Draw(); }
				if (blockFlag[1] == 0) { objBrownBlock[7][23]->Draw(); }
				if (blockFlag[2] == 0) { objBrownBlock[7][29]->Draw(); }
			}
		}
	}
	for (int i = 0; i < 24; i++) {
		if (i < 8 && blockFlag[0] == 1 && EffectPosition[0].y < effectRange[0]) {
			objEffect[i]->Draw();
		}
		if (7 < i && i < 16 && blockFlag[1] == 1 && EffectPosition[8].y < effectRange[1]) {
			objEffect[i]->Draw();
		}
		if (15 < i && blockFlag[2] == 1 && EffectPosition[16].y < effectRange[2]) {
			objEffect[i]->Draw();
		}
	}
}

// ゲッター
void StageOneMap::Getter()
{
	// ゲットポジション
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->GetPosition();
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->GetPosition();
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->GetPosition();
			}
			if (map[y][x] == BROWN)
			{
				objBrownBlock[y][x]->GetPosition();
			}
		}
	}
	for (int i = 0; i < 24; i++) {
		objEffect[i]->GetPosition();
	}

	// ゲットスケール
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->GetScale();
			}
		}
	}
}

// セッター
void StageOneMap::Setter()
{
	// セットポジション
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->SetPosition(MapPosition[y][x]);
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->SetPosition(MapPosition[y][x]);
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->SetPosition(MapPosition[y][x]);
			}
			if (map[y][x] == BROWN)
			{
				objBrownBlock[y][x]->SetPosition(MapPosition[y][x]);
			}
		}
	}
	for (int i = 0; i < 24; i++) {
		objEffect[i]->SetPosition(EffectPosition[i]);
	}

	// セットスケール
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->SetScale({ 0.5f, 0.5f, 0.5f });
			}
		}
	}
}

// エフェクト
void StageOneMap::Effect()
{
	// 茶色ブロック1
	if (blockFlag[0] == 1 && EffectPosition[0].y < effectRange[0]) {
		EffectPosition[0].y += 0.35f;
		EffectPosition[1].x += 0.35f / 1.41421356f;
		EffectPosition[1].y += 0.35f / 1.41421356f;
		EffectPosition[2].x += 0.35f;
		EffectPosition[3].x += 0.35f / 1.41421356f;
		EffectPosition[3].y -= 0.35f / 1.41421356f;
		EffectPosition[4].y -= 0.35f;
		EffectPosition[5].x -= 0.35f / 1.41421356f;
		EffectPosition[5].y -= 0.35f / 1.41421356f;
		EffectPosition[6].x -= 0.35f;
		EffectPosition[7].x -= 0.35f / 1.41421356f;
		EffectPosition[7].y += 0.35f / 1.41421356f;
	}
	// 茶色ブロック2
	if (blockFlag[1] == 1 && EffectPosition[8].y < effectRange[1]) {
		EffectPosition[8].y += 0.35f;
		EffectPosition[9].x += 0.35f / 1.41421356f;
		EffectPosition[9].y += 0.35f / 1.41421356f;
		EffectPosition[10].x += 0.35f;
		EffectPosition[11].x += 0.35f / 1.41421356f;
		EffectPosition[11].y -= 0.35f / 1.41421356f;
		EffectPosition[12].y -= 0.35f;
		EffectPosition[13].x -= 0.35f / 1.41421356f;
		EffectPosition[13].y -= 0.35f / 1.41421356f;
		EffectPosition[14].x -= 0.35f;
		EffectPosition[15].x -= 0.35f / 1.41421356f;
		EffectPosition[15].y += 0.35f / 1.41421356f;
	}
	// 茶色ブロック3
	if (blockFlag[2] == 1 && EffectPosition[16].y < effectRange[2]) {
		EffectPosition[16].y += 0.35f;
		EffectPosition[17].x += 0.35f / 1.41421356f;
		EffectPosition[17].y += 0.35f / 1.41421356f;
		EffectPosition[18].x += 0.35f;
		EffectPosition[19].x += 0.35f / 1.41421356f;
		EffectPosition[19].y -= 0.35f / 1.41421356f;
		EffectPosition[20].y -= 0.35f;
		EffectPosition[21].x -= 0.35f / 1.41421356f;
		EffectPosition[21].y -= 0.35f / 1.41421356f;
		EffectPosition[22].x -= 0.35f;
		EffectPosition[23].x -= 0.35f / 1.41421356f;
		EffectPosition[23].y += 0.35f / 1.41421356f;
	}
}

// リセット
void StageOneMap::Reset()
{
	// ブロック復元
	for (int i = 0; i < 3; i++) {
		blockFlag[i] = 0;
	}
	// エフェクト配置
	for (int i = 0; i < 24; i++) {
		if (i < 8) {
			EffectPosition[i] = { 15, 0, -3 };
		}
		if (7 < i && i < 16) {
			EffectPosition[i] = { 66, 0, -3 };
		}
		if (15 < i) {
			EffectPosition[i] = { 84, 0, -3 };
		}
	}
}