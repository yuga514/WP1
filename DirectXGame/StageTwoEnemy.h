#pragma once

#include "Object3d.h"
#include "SafeDelete.h"

using namespace DirectX;

class StageTwoEnemy
{
public: // メンバ関数
	// コンストラクタ
	StageTwoEnemy();

	// デストラクタ
	~StageTwoEnemy();

	// 初期化
	void Initialize();

	// 更新
	void Update();

	// 描画
	void Draw();

	// ゲッター
	void Getter();

	// セッター
	void Setter();

	// アクション
	void Action();

	// エフェクト
	void Effect();

	// リセット
	void Reset();

	// 座標の取得
	XMFLOAT3 GetPosition1() { return objEnemy[0]->GetPosition(); }
	XMFLOAT3 GetPosition2() { return objEnemy[1]->GetPosition(); }
	XMFLOAT3 GetPosition3() { return objEnemy[2]->GetPosition(); }

	// デスフラグの取得
	unsigned int GetDeathFlag1() { return deathFlag[0]; }
	unsigned int GetDeathFlag2() { return deathFlag[1]; }
	unsigned int GetDeathFlag3() { return deathFlag[2]; }

	// フリーズフラグの設定(True)
	void SetFreezeFlagTrue() { freezeFlag = 1; }

	// フリーズフラグの設定(False)
	void SetFreezeFlagFalse() { freezeFlag = 0; }

	// デスフラグの設定
	void SetDeathFlag1() { deathFlag[0] = 1; }
	void SetDeathFlag2() { deathFlag[1] = 1; }
	void SetDeathFlag3() { deathFlag[2] = 1; }

	// エフェクトの設定
	void SetEffect1();
	void SetEffect2();
	void SetEffect3();

private: // メンバ変数
	bool freezeFlag = 0;
	bool moveFlag[3] = { 0,0,0 };
	bool deathFlag[3] = { 0,0,0 };

	XMFLOAT3 EnemyPosition[3] = { { 21, 0, 0 },{ 45, 0, 0 }, { 72, 0, 0 } };
	XMFLOAT3 EnemyRotation[3] = { { 0,0,0 },{ 0,0,0 },{ 0,0,0 } };

	XMFLOAT3 EffectPosition[24] = {};
	float effectRange[3] = { 4.5f, 4.5f, 4.5f };

	Model* modelBrownEnemy = nullptr;
	Model* modelGrayEnemy = nullptr;
	Model* modelYellowBall = nullptr;

	Object3d* objEnemy[3] = {};
	Object3d* objEffect[24] = {};
};