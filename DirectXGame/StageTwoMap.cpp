#include "StageTwoMap.h"

// コンストラクタ
StageTwoMap::StageTwoMap()
{
}

// デストラクタ
StageTwoMap::~StageTwoMap()
{
	safe_delete(modelBlock);
	safe_delete(modelDoor);
	safe_delete(modelOperation);
	safe_delete(modelGrayBlock);
	safe_delete(modelGrayBall);

	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				safe_delete(objBlock[y][x]);
			}
			if (map[y][x] == DOOR)
			{
				safe_delete(objDoor[y][x]);
			}
			if (map[y][x] == OPERATION)
			{
				safe_delete(objOperation[y][x]);
			}
			if (map[y][x] == GRAY)
			{
				safe_delete(objGrayBlock[y][x]);
			}
		}
	}
	for (int i = 0; i < 32; i++) {
		safe_delete(objEffect[i]);
	}
}

// 初期化
void StageTwoMap::Initialize()
{
	// モデル読み込み
	modelBlock = Model::CreateFromOBJ("block");
	modelDoor = Model::CreateFromOBJ("door");
	modelOperation = Model::CreateFromOBJ("operation");
	modelGrayBlock = Model::CreateFromOBJ("grayBlock");
	modelGrayBall = Model::CreateFromOBJ("grayBall");

	// 3Dオブジェクト生成
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x] = Object3d::Create(modelBlock);
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x] = Object3d::Create(modelDoor);
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x] = Object3d::Create(modelOperation);
			}
			if (map[y][x] == GRAY)
			{
				objGrayBlock[y][x] = Object3d::Create(modelGrayBlock);
			}
		}
	}
	for (int i = 0; i < 32; i++) {
		objEffect[i] = Object3d::Create(modelGrayBall);
	}

	// マップチップ配置
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			MapPosition[y][x].x = 3.0f * x - 3.0f;
			MapPosition[y][x].y = -3.0f * y + 19.8f;
		}
	}

	// エフェクト配置
	for (int i = 0; i < 32; i++) {
		if (i < 8) {
			EffectPosition[i] = { 81, 9, -3 };
		}
		if (7 < i && i < 16) {
			EffectPosition[i] = { 81, 6, -3 };
		}
		if (15 < i && i < 24) {
			EffectPosition[i] = { 81, 3, -3 };
		}
		if (23 < i) {
			EffectPosition[i] = { 81, 0, -3 };
		}
	}
}

// 更新
void StageTwoMap::Update()
{
	// ゲッター
	Getter();

	// エフェクト
	Effect();

	// セッター
	Setter();

	// アップデート
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->Update();
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->Update();
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->Update();
			}
			if (map[y][x] == GRAY)
			{
				objGrayBlock[y][x]->Update();
			}
		}
	}
	for (int i = 0; i < 32; i++) {
		objEffect[i]->Update();
	}
}

// 描画
void StageTwoMap::Draw()
{
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->Draw();
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->Draw();
			}
			if (map[y][x] == OPERATION)
			{
				if (operationFlag == 1) { objOperation[5][31]->Draw(); }
			}
			if (map[y][x] == GRAY)
			{
				if (blockFlag[0] == 0) { objGrayBlock[4][28]->Draw(); }
				if (blockFlag[1] == 0) { objGrayBlock[5][28]->Draw(); }
				if (blockFlag[2] == 0) { objGrayBlock[6][28]->Draw(); }
				if (blockFlag[3] == 0) { objGrayBlock[7][28]->Draw(); }
			}
		}
	}
	for (int i = 0; i < 32; i++) {
		if (i < 8 && blockFlag[0] == 1 && EffectPosition[0].y < effectRange[0]) {
			objEffect[i]->Draw();
		}
		if (7 < i && i < 16 && blockFlag[1] == 1 && EffectPosition[8].y < effectRange[1]) {
			objEffect[i]->Draw();
		}
		if (15 < i && i < 24 && blockFlag[2] == 1 && EffectPosition[16].y < effectRange[2]) {
			objEffect[i]->Draw();
		}
		if (23 < i && blockFlag[3] == 1 && EffectPosition[24].y < effectRange[3]) {
			objEffect[i]->Draw();
		}
	}
}

// ゲッター
void StageTwoMap::Getter()
{
	// ゲットポジション
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->GetPosition();
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->GetPosition();
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->GetPosition();
			}
			if (map[y][x] == GRAY)
			{
				objGrayBlock[y][x]->GetPosition();
			}
		}
	}
	for (int i = 0; i < 32; i++) {
		objEffect[i]->GetPosition();
	}

	// ゲットスケール
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->GetScale();
			}
		}
	}
}

// セッター
void StageTwoMap::Setter()
{
	// セットポジション
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->SetPosition(MapPosition[y][x]);
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->SetPosition(MapPosition[y][x]);
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->SetPosition(MapPosition[y][x]);
			}
			if (map[y][x] == GRAY)
			{
				objGrayBlock[y][x]->SetPosition(MapPosition[y][x]);
			}
		}
	}
	for (int i = 0; i < 32; i++) {
		objEffect[i]->SetPosition(EffectPosition[i]);
	}

	// セットスケール
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->SetScale({ 0.5f, 0.5f, 0.5f });
			}
		}
	}
}

// エフェクト
void StageTwoMap::Effect()
{
	// 灰色ブロック1
	if (blockFlag[0] == 1 && EffectPosition[0].y < effectRange[0]) {
		EffectPosition[0].y += 0.35f;
		EffectPosition[1].x += 0.35f / 1.41421356f;
		EffectPosition[1].y += 0.35f / 1.41421356f;
		EffectPosition[2].x += 0.35f;
		EffectPosition[3].x += 0.35f / 1.41421356f;
		EffectPosition[3].y -= 0.35f / 1.41421356f;
		EffectPosition[4].y -= 0.35f;
		EffectPosition[5].x -= 0.35f / 1.41421356f;
		EffectPosition[5].y -= 0.35f / 1.41421356f;
		EffectPosition[6].x -= 0.35f;
		EffectPosition[7].x -= 0.35f / 1.41421356f;
		EffectPosition[7].y += 0.35f / 1.41421356f;
	}
	// 灰色ブロック2
	if (blockFlag[1] == 1 && EffectPosition[8].y < effectRange[1]) {
		EffectPosition[8].y += 0.35f;
		EffectPosition[9].x += 0.35f / 1.41421356f;
		EffectPosition[9].y += 0.35f / 1.41421356f;
		EffectPosition[10].x += 0.35f;
		EffectPosition[11].x += 0.35f / 1.41421356f;
		EffectPosition[11].y -= 0.35f / 1.41421356f;
		EffectPosition[12].y -= 0.35f;
		EffectPosition[13].x -= 0.35f / 1.41421356f;
		EffectPosition[13].y -= 0.35f / 1.41421356f;
		EffectPosition[14].x -= 0.35f;
		EffectPosition[15].x -= 0.35f / 1.41421356f;
		EffectPosition[15].y += 0.35f / 1.41421356f;
	}
	// 灰色ブロック3
	if (blockFlag[2] == 1 && EffectPosition[16].y < effectRange[2]) {
		EffectPosition[16].y += 0.35f;
		EffectPosition[17].x += 0.35f / 1.41421356f;
		EffectPosition[17].y += 0.35f / 1.41421356f;
		EffectPosition[18].x += 0.35f;
		EffectPosition[19].x += 0.35f / 1.41421356f;
		EffectPosition[19].y -= 0.35f / 1.41421356f;
		EffectPosition[20].y -= 0.35f;
		EffectPosition[21].x -= 0.35f / 1.41421356f;
		EffectPosition[21].y -= 0.35f / 1.41421356f;
		EffectPosition[22].x -= 0.35f;
		EffectPosition[23].x -= 0.35f / 1.41421356f;
		EffectPosition[23].y += 0.35f / 1.41421356f;
	}
	// 灰色ブロック4
	if (blockFlag[3] == 1 && EffectPosition[24].y < effectRange[3]) {
		EffectPosition[24].y += 0.35f;
		EffectPosition[25].x += 0.35f / 1.41421356f;
		EffectPosition[25].y += 0.35f / 1.41421356f;
		EffectPosition[26].x += 0.35f;
		EffectPosition[27].x += 0.35f / 1.41421356f;
		EffectPosition[27].y -= 0.35f / 1.41421356f;
		EffectPosition[28].y -= 0.35f;
		EffectPosition[29].x -= 0.35f / 1.41421356f;
		EffectPosition[29].y -= 0.35f / 1.41421356f;
		EffectPosition[30].x -= 0.35f;
		EffectPosition[31].x -= 0.35f / 1.41421356f;
		EffectPosition[31].y += 0.35f / 1.41421356f;
	}
}

// リセット
void StageTwoMap::Reset()
{
	// ブロック復元
	for (int i = 0; i < 4; i++) {
		blockFlag[i] = 0;
	}
	// エフェクト配置
	for (int i = 0; i < 32; i++) {
		if (i < 8) {
			EffectPosition[i] = { 81, 9, -3 };
		}
		if (7 < i && i < 16) {
			EffectPosition[i] = { 81, 6, -3 };
		}
		if (15 < i && i < 24) {
			EffectPosition[i] = { 81, 3, -3 };
		}
		if (23 < i) {
			EffectPosition[i] = { 81, 0, -3 };
		}
	}
}