#include "StageOneEnemy.h"

// コンストラクタ
StageOneEnemy::StageOneEnemy()
{
}

// デストラクタ
StageOneEnemy::~StageOneEnemy()
{
	safe_delete(modelBrownEnemy);
	safe_delete(modelGrayEnemy);
	safe_delete(modelYellowBall);

	for (int i = 0; i < 4; i++) {
		safe_delete(objEnemy[i]);
	}
	for (int i = 0; i < 32; i++) {
		safe_delete(objEffect[i]);
	}
}

// 初期化
void StageOneEnemy::Initialize()
{
	// モデル読み込み
	modelBrownEnemy = Model::CreateFromOBJ("brownEnemy");
	modelGrayEnemy = Model::CreateFromOBJ("grayEnemy");
	modelYellowBall = Model::CreateFromOBJ("yellowBall");

	// 3Dオブジェクト生成
	objEnemy[0] = Object3d::Create(modelBrownEnemy);
	objEnemy[1] = Object3d::Create(modelBrownEnemy);
	objEnemy[2] = Object3d::Create(modelGrayEnemy);
	objEnemy[3] = Object3d::Create(modelGrayEnemy);
	for (int i = 0; i < 32; i++) {
		objEffect[i] = Object3d::Create(modelYellowBall);
	}

	// エフェクト配置
	for (int i = 0; i < 32; i++) {
		EffectPosition[i] = { -10.0f, -10.0f, 0 };
	}
}

// 更新
void StageOneEnemy::Update()
{
	// ゲッター
	Getter();

	// アクション
	if (freezeFlag == 0) { Action(); }

	// エフェクト
	Effect();

	// セッター
	Setter();

	// アップデート
	for (int i = 0; i < 4; i++) {
		objEnemy[i]->Update();
	}
	for (int i = 0; i < 32; i++) {
		objEffect[i]->Update();
	}
}

// 描画
void StageOneEnemy::Draw()
{
	for (int i = 0; i < 4; i++) {
		if (deathFlag[i] == 0) {
			objEnemy[i]->Draw();
		}
	}
	for (int i = 0; i < 32; i++) {
		if (i < 8 && deathFlag[0] == 1 && EffectPosition[0].y < effectRange[0]) {
			objEffect[i]->Draw();
		}
		if (7 < i && i < 16 && deathFlag[1] == 1 && EffectPosition[8].y < effectRange[1]) {
			objEffect[i]->Draw();
		}
		if (15 < i && i < 24 && deathFlag[2] == 1 && EffectPosition[16].y < effectRange[2]) {
			objEffect[i]->Draw();
		}
		if (23 < i && deathFlag[3] == 1 && EffectPosition[24].y < effectRange[3]) {
			objEffect[i]->Draw();
		}
	}
}

// ゲッター
void StageOneEnemy::Getter()
{
	// ゲットポジション
	for (int i = 0; i < 4; i++) {
		objEnemy[i]->GetPosition();
	}
	for (int i = 0; i < 32; i++) {
		objEffect[i]->GetPosition();
	}

	// ゲットローテーション
	for (int i = 0; i < 4; i++) {
		objEnemy[i]->GetRotation();
	}
}

// セッター
void StageOneEnemy::Setter()
{
	// セットポジション
	for (int i = 0; i < 4; i++) {
		objEnemy[i]->SetPosition(EnemyPosition[i]);
	}
	for (int i = 0; i < 32; i++) {
		objEffect[i]->SetPosition(EffectPosition[i]);
	}

	// セットローテーション
	for (int i = 0; i < 4; i++) {
		objEnemy[i]->SetRotation(EnemyRotation[i]);
	}
}

// アクション
void StageOneEnemy::Action()
{
	for (int i = 0; i < 4; i++) {
		// 生きているとき
		if (deathFlag[i] == 0) {
			// 左移動
			if (moveFlag[i] == 0) {
				EnemyPosition[i].x -= 0.1f;
			}

			// 右移動
			if (moveFlag[i] == 1) {
				EnemyPosition[i].x += 0.1f;
			}

			// 回転
			EnemyRotation[i].y += 3.6f;
			if (EnemyRotation[i].y == 360) {
				EnemyRotation[i].y = 0.0f;
			}

			// 方向転換
			if (EnemyPosition[0].x < 18 || EnemyPosition[1].x < 36 || EnemyPosition[2].x < 51 || EnemyPosition[3].x < 69) {
				moveFlag[i] = 1;
			}
			if (30 < EnemyPosition[0].x || 48 < EnemyPosition[1].x || 63 < EnemyPosition[2].x || 81 < EnemyPosition[3].x) {
				moveFlag[i] = 0;
			}
		}
	}
}

// エフェクト
void StageOneEnemy::Effect()
{
	// 敵1
	if (deathFlag[0] == 1 && EffectPosition[0].y < effectRange[0]) {
		EffectPosition[0].y += 0.35f;
		EffectPosition[1].x += 0.35f / 1.41421356f;
		EffectPosition[1].y += 0.35f / 1.41421356f;
		EffectPosition[2].x += 0.35f;
		EffectPosition[3].x += 0.35f / 1.41421356f;
		EffectPosition[3].y -= 0.35f / 1.41421356f;
		EffectPosition[4].y -= 0.35f;
		EffectPosition[5].x -= 0.35f / 1.41421356f;
		EffectPosition[5].y -= 0.35f / 1.41421356f;
		EffectPosition[6].x -= 0.35f;
		EffectPosition[7].x -= 0.35f / 1.41421356f;
		EffectPosition[7].y += 0.35f / 1.41421356f;
	}
	// 敵2
	if (deathFlag[1] == 1 && EffectPosition[8].y < effectRange[1]) {
		EffectPosition[8].y += 0.35f;
		EffectPosition[9].x += 0.35f / 1.41421356f;
		EffectPosition[9].y += 0.35f / 1.41421356f;
		EffectPosition[10].x += 0.35f;
		EffectPosition[11].x += 0.35f / 1.41421356f;
		EffectPosition[11].y -= 0.35f / 1.41421356f;
		EffectPosition[12].y -= 0.35f;
		EffectPosition[13].x -= 0.35f / 1.41421356f;
		EffectPosition[13].y -= 0.35f / 1.41421356f;
		EffectPosition[14].x -= 0.35f;
		EffectPosition[15].x -= 0.35f / 1.41421356f;
		EffectPosition[15].y += 0.35f / 1.41421356f;
	}
	// 敵3
	if (deathFlag[2] == 1 && EffectPosition[16].y < effectRange[2]) {
		EffectPosition[16].y += 0.35f;
		EffectPosition[17].x += 0.35f / 1.41421356f;
		EffectPosition[17].y += 0.35f / 1.41421356f;
		EffectPosition[18].x += 0.35f;
		EffectPosition[19].x += 0.35f / 1.41421356f;
		EffectPosition[19].y -= 0.35f / 1.41421356f;
		EffectPosition[20].y -= 0.35f;
		EffectPosition[21].x -= 0.35f / 1.41421356f;
		EffectPosition[21].y -= 0.35f / 1.41421356f;
		EffectPosition[22].x -= 0.35f;
		EffectPosition[23].x -= 0.35f / 1.41421356f;
		EffectPosition[23].y += 0.35f / 1.41421356f;
	}
	// 敵4
	if (deathFlag[3] == 1 && EffectPosition[24].y < effectRange[3]) {
		EffectPosition[24].y += 0.35f;
		EffectPosition[25].x += 0.35f / 1.41421356f;
		EffectPosition[25].y += 0.35f / 1.41421356f;
		EffectPosition[26].x += 0.35f;
		EffectPosition[27].x += 0.35f / 1.41421356f;
		EffectPosition[27].y -= 0.35f / 1.41421356f;
		EffectPosition[28].y -= 0.35f;
		EffectPosition[29].x -= 0.35f / 1.41421356f;
		EffectPosition[29].y -= 0.35f / 1.41421356f;
		EffectPosition[30].x -= 0.35f;
		EffectPosition[31].x -= 0.35f / 1.41421356f;
		EffectPosition[31].y += 0.35f / 1.41421356f;
	}
}

// エフェクトの設定
void StageOneEnemy::SetEffect1()
{
	for (int i = 0; i < 8; i++) {
		EffectPosition[i].x = EnemyPosition[0].x;
		EffectPosition[i].y = EnemyPosition[0].y;
		EffectPosition[i].z = -3.0f;
	}
}

void StageOneEnemy::SetEffect2()
{
	for (int i = 8; i < 16; i++) {
		EffectPosition[i].x = EnemyPosition[1].x;
		EffectPosition[i].y = EnemyPosition[1].y;
		EffectPosition[i].z = -3.0f;
	}
}

void StageOneEnemy::SetEffect3()
{
	for (int i = 16; i < 24; i++) {
		EffectPosition[i].x = EnemyPosition[2].x;
		EffectPosition[i].y = EnemyPosition[2].y;
		EffectPosition[i].z = -3.0f;
	}
}

void StageOneEnemy::SetEffect4()
{
	for (int i = 24; i < 32; i++) {
		EffectPosition[i].x = EnemyPosition[3].x;
		EffectPosition[i].y = EnemyPosition[3].y;
		EffectPosition[i].z = -3.0f;
	}
}

void StageOneEnemy::Reset()
{
	freezeFlag = 0;
	for (int i = 0; i < 4; i++) {
		moveFlag[i] = 0;
		deathFlag[i] = 0;
	}
	EnemyPosition[0] = { 27, 0, 0 };
	EnemyPosition[1] = { 45, 12, 0 };
	EnemyPosition[2] = { 60, 0, 0 };
	EnemyPosition[3] = { 78, 0, 0 };
	for (int i = 0; i < 4; i++) {
		EnemyRotation[i] = { 0,0,0 };
	}
	// エフェクト配置
	for (int i = 0; i < 32; i++) {
		EffectPosition[i] = { -10.0f, -10.0f, 0 };
	}
}