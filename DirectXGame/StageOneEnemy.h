#pragma once

#include "Object3d.h"
#include "SafeDelete.h"

using namespace DirectX;

class StageOneEnemy
{
public: // メンバ関数
	// コンストラクタ
	StageOneEnemy();

	// デストラクタ
	~StageOneEnemy();

	// 初期化
	void Initialize();

	// 更新
	void Update();

	// 描画
	void Draw();

	// ゲッター
	void Getter();

	// セッター
	void Setter();

	// アクション
	void Action();

	// エフェクト
	void Effect();

	// リセット
	void Reset();

	// 座標の取得
	XMFLOAT3 GetPosition1() { return objEnemy[0]->GetPosition(); }
	XMFLOAT3 GetPosition2() { return objEnemy[1]->GetPosition(); }
	XMFLOAT3 GetPosition3() { return objEnemy[2]->GetPosition(); }
	XMFLOAT3 GetPosition4() { return objEnemy[3]->GetPosition(); }

	// デスフラグの取得
	unsigned int GetDeathFlag1() { return deathFlag[0]; }
	unsigned int GetDeathFlag2() { return deathFlag[1]; }
	unsigned int GetDeathFlag3() { return deathFlag[2]; }
	unsigned int GetDeathFlag4() { return deathFlag[3]; }

	// フリーズフラグの設定(True)
	void SetFreezeFlagTrue() { freezeFlag = 1; }

	// フリーズフラグの設定(False)
	void SetFreezeFlagFalse() { freezeFlag = 0; }

	// デスフラグの設定
	void SetDeathFlag1() { deathFlag[0] = 1; }
	void SetDeathFlag2() { deathFlag[1] = 1; }
	void SetDeathFlag3() { deathFlag[2] = 1; }
	void SetDeathFlag4() { deathFlag[3] = 1; }

	// エフェクトの設定
	void SetEffect1();
	void SetEffect2();
	void SetEffect3();
	void SetEffect4();

private: // メンバ変数
	bool freezeFlag = 0;
	bool moveFlag[4] = { 0,0,0,0 };
	bool deathFlag[4] = { 0,0,0,0 };

	XMFLOAT3 EnemyPosition[4] = { { 27, 0, 0 },{ 45, 12, 0 },{ 60, 0, 0 },{ 78, 0, 0 } };
	XMFLOAT3 EnemyRotation[4] = { { 0,0,0 },{ 0,0,0 },{ 0,0,0 },{ 0,0,0 } };

	XMFLOAT3 EffectPosition[32] = {};
	float effectRange[4] = { 4.5f, 16.5f, 4.5f, 4.5f };

	Model* modelBrownEnemy = nullptr;
	Model* modelGrayEnemy = nullptr;
	Model* modelYellowBall = nullptr;

	Object3d* objEnemy[4] = {};
	Object3d* objEffect[32] = {};
};