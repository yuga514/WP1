#include "GameclearPlayer.h"

// コンストラクタ
GameclearPlayer::GameclearPlayer()
{
}

// デストラクタ
GameclearPlayer::~GameclearPlayer()
{
	safe_delete(modelPlayerRight);
	safe_delete(objPlayerRight);
	safe_delete(simpleMap);
}

// 初期化
void GameclearPlayer::Initialize(Input* input)
{
	this->input = input;

	// カメラ生成
	camera = new DebugCamera(WinApp::window_width, WinApp::window_height, input);

	// 3Dオブジェクトにカメラをセット
	Object3d::SetCamera(camera);

	// カメラ注視点をセット
	camera->SetDistance(3.0f);

	// モデル読み込み
	modelPlayerRight = Model::CreateFromOBJ("playerRight");

	// 3Dオブジェクト生成
	objPlayerRight = Object3d::Create(modelPlayerRight);

	// シンプルマップ生成
	simpleMap = new SimpleMap();
	simpleMap->Initialize();
}

// 更新
void GameclearPlayer::Update()
{
	// ゲッター
	Getter();

	// アクション
	Action();

	// セッター
	Setter();

	// アップデート
	camera->Update();

	objPlayerRight->Update();
	simpleMap->Update();
}

// 描画
void GameclearPlayer::Draw()
{
	objPlayerRight->Draw();
	simpleMap->Draw();
}

// ゲッター
void GameclearPlayer::Getter()
{
	// ゲットターゲット
	camera->GetTarget();

	// ゲットポジション
	objPlayerRight->GetPosition();

	// ゲットローテーション
	objPlayerRight->GetRotation();
}

// セッター
void GameclearPlayer::Setter()
{
	// セットターゲット
	camera->SetTarget(CameraPosition);

	// セットポジション
	objPlayerRight->SetPosition(PlayerPosition);

	// セットローテーション
	objPlayerRight->SetRotation(PlayerRotation);
}

// アクション
void GameclearPlayer::Action()
{
	// 着地
	if (PlayerPosition.y == 0) {
		landFlag = 1;
	}
	if (0 < PlayerPosition.y) {
		landFlag = 0;
	}

	// 落下処理
	PlayerPosition.y -= jumpPower;

	// 落下加速度を加える
	jumpPower += 0.01f;

	// 地面についていたら止まる
	if (PlayerPosition.y < 0) {
		PlayerPosition.y = 0.0f;
	}

	// 左移動
	if (moveFlag == 0) {
		PlayerPosition.x -= 0.2f;
	}

	// 右移動
	if (moveFlag == 1) {
		PlayerPosition.x += 0.2f;
	}

	// ジャンプ
	if ((PlayerPosition.x < 6 || 20 < PlayerPosition.x && PlayerPosition.x < 22 || 36 < PlayerPosition.x) && landFlag == 1) {
		jumpPower = -0.35f;
	}

	// 左回転
	if (moveFlag == 0) {
		PlayerRotation.x -= 3.6f;
	}

	// 右回転
	if (moveFlag == 1) {
		PlayerRotation.x += 3.6f;
	}

	// 方向転換
	if (PlayerPosition.x < 6) {
		moveFlag = 1;
	}
	if (36 < PlayerPosition.x) {
		moveFlag = 0;
	}
}

// リセット
void GameclearPlayer::Reset()
{
	jumpPower = 0;
	landFlag = 0;
	moveFlag = 0;
	CameraPosition = { 21, 8.3f, -20 };
	PlayerPosition = { 21,0,0 };
	PlayerRotation = { 0,90,0 };
}