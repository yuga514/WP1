#include "StageTwoPlayer.h"

// コンストラクタ
StageTwoPlayer::StageTwoPlayer()
{
}

// デストラクタ
StageTwoPlayer::~StageTwoPlayer()
{
	safe_delete(modelPlayerRight);
	safe_delete(modelPlayerLeft);
	safe_delete(modelAttack1);
	safe_delete(modelAttack2);
	safe_delete(modelYellowBall);
	safe_delete(objPlayerRight);
	safe_delete(objPlayerLeft);
	safe_delete(objAttack1);
	safe_delete(objAttack2);
	for (int i = 0; i < 8; i++) {
		safe_delete(objEffect[i]);
	}
	safe_delete(stageTwoMap);
}

// 初期化
void StageTwoPlayer::Initialize(Input* input)
{
	this->input = input;

	// カメラ生成
	camera = new DebugCamera(WinApp::window_width, WinApp::window_height, input);

	// 3Dオブジェクトにカメラをセット
	Object3d::SetCamera(camera);

	// カメラ注視点をセット
	camera->SetDistance(3.0f);

	// モデル読み込み
	modelPlayerRight = Model::CreateFromOBJ("playerRight");
	modelPlayerLeft = Model::CreateFromOBJ("playerLeft");
	modelAttack1 = Model::CreateFromOBJ("attack1");
	modelAttack2 = Model::CreateFromOBJ("attack2");
	modelYellowBall = Model::CreateFromOBJ("yellowBall");

	// 3Dオブジェクト生成
	objPlayerRight = Object3d::Create(modelPlayerRight);
	objPlayerLeft = Object3d::Create(modelPlayerLeft);
	objAttack1 = Object3d::Create(modelAttack1);
	objAttack2 = Object3d::Create(modelAttack2);
	for (int i = 0; i < 8; i++) {
		objEffect[i] = Object3d::Create(modelYellowBall);
	}

	// エフェクト配置
	for (int i = 0; i < 8; i++) {
		EffectPosition[i] = { -10.0f, -10.0f, 0 };
	}

	// ステージ2マップ生成
	stageTwoMap = new StageTwoMap();
	stageTwoMap->Initialize();
}

// 更新
void StageTwoPlayer::Update()
{
	// ゲッター
	Getter();

	// アクション
	if (freezeFlag == 0 && deathFlag == 0) { Action(); }
	if (deathFlag == 1 && 1 < damageCount) { PlayerPosition.y += 0.05f; }

	// ダメージを受けたら
	if (0 < damageCount) { damageCount--; }

	// 当たり判定
	Collision();

	// エフェクト
	Effect();

	// セッター
	Setter();

	// アップデート
	camera->Update();

	objPlayerRight->Update();
	objPlayerLeft->Update();
	objAttack1->Update();
	objAttack2->Update();
	for (int i = 0; i < 8; i++) {
		objEffect[i]->Update();
	}
	stageTwoMap->Update();
}

// 描画
void StageTwoPlayer::Draw()
{
	if ((deathFlag == 0 || deathFlag == 1 && damageCount != 0) && damageCount % 2 == 0) {
		if (direction == 0) {
			objPlayerRight->Draw();
		}
		if (direction == 1) {
			objPlayerLeft->Draw();
		}
		if (attackFlag == 1) {
			objAttack1->Draw();
		}
		if (attackFlag == 2) {
			objAttack2->Draw();
		}
	}
	for (int i = 0; i < 8; i++) {
		if (deathFlag == 1 && EffectPosition[0].y < effectRange) {
			objEffect[i]->Draw();
		}
	}
	stageTwoMap->Draw();
}

// ゲッター
void StageTwoPlayer::Getter()
{
	// ゲットターゲット
	camera->GetTarget();

	// ゲットポジション
	objPlayerRight->GetPosition();
	objPlayerLeft->GetPosition();
	objAttack1->GetPosition();
	objAttack2->GetPosition();
	for (int i = 0; i < 8; i++) {
		objEffect[i]->GetPosition();
	}

	// ゲットローテーション
	objPlayerRight->GetRotation();
	objPlayerLeft->GetRotation();

	// ゲットスケール
	objPlayerRight->GetScale();
	objPlayerLeft->GetScale();
}

// セッター
void StageTwoPlayer::Setter()
{
	// セットターゲット
	camera->SetTarget(CameraPosition);

	// セットポジション
	objPlayerRight->SetPosition(PlayerPosition);
	objPlayerLeft->SetPosition(PlayerPosition);
	objAttack1->SetPosition(PlayerPosition);
	objAttack2->SetPosition(PlayerPosition);
	for (int i = 0; i < 8; i++) {
		objEffect[i]->SetPosition(EffectPosition[i]);
	}

	// セットローテーション
	objPlayerRight->SetRotation(PlayerRotation);
	objPlayerLeft->SetRotation(PlayerRotation);

	// セットスケール
	objPlayerRight->SetScale(PlayerScale);
	objPlayerLeft->SetScale(PlayerScale);
}

// アクション
void StageTwoPlayer::Action()
{
	// スティックの入力を取得
	Input::StickMove stickMove = input->GetStickMove();

	// 落下処理
	PlayerPosition.y -= jumpPower;

	// 落下加速度を加える
	jumpPower += 0.01f;

	// 右移動
	if (input->PushKey(DIK_RIGHT) || 42767 < stickMove.lX && input->GetDevJoyStick()) {
		if (72 < PlayerPosition.x) {
			CameraPosition.x = 72.0f;
		}
		if (21 < PlayerPosition.x && PlayerPosition.x < 72) {
			CameraPosition.x += speed;
		}
		PlayerPosition.x += speed;
		PlayerRotation.z -= 3.6f;
		direction = 0;
	}

	// 左移動
	if (input->PushKey(DIK_LEFT) || stickMove.lX < 22767 && input->GetDevJoyStick()) {
		if (PlayerPosition.x < 21) {
			CameraPosition.x = 21.0f;
		}
		if (21 < PlayerPosition.x && PlayerPosition.x < 72) {
			CameraPosition.x -= speed;
		}
		PlayerPosition.x -= speed;
		PlayerRotation.z += 3.6f;
		direction = 1;
	}

	// ダッシュ
	if (jumpCount == 0) {
		if (input->PushKey(DIK_LSHIFT) || input->PushButton(4)) {
			speed = 0.2f;
		}
		else if (input->PushKey(DIK_LSHIFT) == 0 || input->PushButton(4) == 0) {
			speed = 0.1f;
		}
	}

	// 0段目の地面についていたら止まる
	if (PlayerPosition.y < 0) {
		PlayerPosition.y = 0.0f;
		Land();
	}
	// 1個目の4段目の地面についていたら止まる
	if (PlayerPosition.x < 14 && PlayerPosition.y < 12) {
		PlayerPosition.y = 12.0f;
		Land();
	}
	// 1個目の2段目の地面についていたら止まる
	if (PlayerPosition.x < 17 && PlayerPosition.y < 6) {
		PlayerPosition.y = 6.0f;
		Land();
	}
	// 2個目の4段目の地面についていたら止まる
	if (25 < PlayerPosition.x && PlayerPosition.x < 35 && PlayerPosition.y < 12) {
		PlayerPosition.y = 12.0f;
		Land();
	}
	// 2個目の2段目の地面についていたら止まる
	if (34 < PlayerPosition.x && PlayerPosition.x < 38 && PlayerPosition.y < 6) {
		PlayerPosition.y = 6.0f;
		Land();
	}
	// 3個目の4段目の地面についていたら止まる
	if (49 < PlayerPosition.x && PlayerPosition.x < 59 && PlayerPosition.y < 12) {
		PlayerPosition.y = 12.0f;
		Land();
	}
	// 3個目の2段目の地面についていたら止まる
	if (58 < PlayerPosition.x && PlayerPosition.x < 62 && PlayerPosition.y < 6) {
		PlayerPosition.y = 6.0f;
		Land();
	}
	// 4個目の4段目の地面についていたら止まる
	if (76 < PlayerPosition.x && PlayerPosition.x < 80 && PlayerPosition.y < 12) {
		PlayerPosition.y = 12.0f;
		Land();
	}
	// 4個目の4段目の地面についていたら止まる（灰色ブロックの上）
	if (79 < PlayerPosition.x && PlayerPosition.x < 83 && PlayerPosition.y < 12 && stageTwoMap->GetBlockFlag1() == 0) {
		PlayerPosition.y = 12.0f;
		CrushBlockLand();
	}
	// 3段目の地面についていたら止まる（灰色ブロックの上）
	if (79 < PlayerPosition.x && PlayerPosition.x < 83 && PlayerPosition.y < 9 && stageTwoMap->GetBlockFlag2() == 0) {
		PlayerPosition.y = 9.0f;
		CrushBlockLand();
	}

	// ジャンプ
	if ((input->TriggerKey(DIK_Z) || input->TriggerButton(0)) && jumpCount < 2) {
		if (speed == 0.2f) {
			jumpPower = -0.35f;
		}
		else if (speed == 0.1f) {
			jumpPower = -0.3f;
		}
		jumpCount += 1;
	}
	if (jumpCount == 2) {
		if (direction == 0) {
			PlayerRotation.z -= 36.0f;
		}
		else if (direction == 1) {
			PlayerRotation.z += 36.0f;
		}
	}

	// 攻撃
	if ((input->TriggerKey(DIK_X) || input->TriggerButton(1)) && attackFlag == 0) {
		if ((input->PushKey(DIK_DOWN) || 42767 < stickMove.lY && input->GetDevJoyStick()) && jumpCount == 2) {
			attackFlag = 2;
		}
		else {
			attackFlag = 1;
		}
	}
	if (attackFlag == 2) {
		PlayerRotation.y += 36.0f;
		PlayerPosition.y -= 1.0f;
		PlayerRotation.z = 0.0f;
	}
	else if (attackFlag == 1) {
		PlayerRotation.y += 36.0f;
		PlayerRotation.z = 0.0f;
		if (PlayerRotation.y == 720) {
			PlayerRotation.y = 0.0f;
			attackFlag = 0;
		}
	}

	// プレイヤーが凹んでいたら
	if (PlayerScale.y < 1) {
		PlayerScale.x -= 0.1f;
		PlayerScale.y += 0.1f;
		PlayerScale.z -= 0.1f;
	}
}

// 当たり判定
void StageTwoPlayer::Collision()
{
	// 画面外に出ないようにする
	if (PlayerPosition.x < 0) {
		PlayerPosition.x = 0.0f;
	}
	if (93 < PlayerPosition.x) {
		PlayerPosition.x = 93.0f;
	}

	// 7個目のブロックの当たり判定（右）
	if (75 < PlayerPosition.x && PlayerPosition.x < 76 && PlayerPosition.y < 12 ||
		// 8個目のブロックの当たり判定（右）
		81 < PlayerPosition.x && PlayerPosition.x < 82 && 0 < PlayerPosition.y) {
		PlayerPosition.x -= speed;
	}
	// 3個目のブロックの当たり判定（右）
	if (24 < PlayerPosition.x && PlayerPosition.x < 25 && PlayerPosition.y < 12 ||
		// 5個目のブロックの当たり判定（右）
		48 < PlayerPosition.x && PlayerPosition.x < 49 && PlayerPosition.y < 12) {
		CameraPosition.x -= speed;
		PlayerPosition.x -= speed;
	}

	// 1個目のブロックの当たり判定（左）
	if (14 < PlayerPosition.x && PlayerPosition.x < 15 && PlayerPosition.y < 12 ||
		// 2個目のブロックの当たり判定（左）
		17 < PlayerPosition.x && PlayerPosition.x < 18 && PlayerPosition.y < 6 ||
		// 7個目のブロックの当たり判定（左）
		80 < PlayerPosition.x && PlayerPosition.x < 81 && PlayerPosition.y < 12 ||
		// 8個目のブロックの当たり判定（左）
		86 < PlayerPosition.x && PlayerPosition.x < 87 && 0 < PlayerPosition.y) {
		PlayerPosition.x += speed;
	}
	// 3個目のブロックの当たり判定（左）
	if (35 < PlayerPosition.x && PlayerPosition.x < 36 && PlayerPosition.y < 12 ||
		// 4個目のブロックの当たり判定（左）
		38 < PlayerPosition.x && PlayerPosition.x < 39 && PlayerPosition.y < 6 ||
		// 5個目のブロックの当たり判定（左）
		59 < PlayerPosition.x && PlayerPosition.x < 60 && PlayerPosition.y < 12 ||
		// 6個目のブロックの当たり判定（左）
		62 < PlayerPosition.x && PlayerPosition.x < 63 && PlayerPosition.y < 6) {
		CameraPosition.x += speed;
		PlayerPosition.x += speed;
	}

	// 1個目の4段目の地面から落ちてしまった場合（右）
	if (14 < PlayerPosition.x && PlayerPosition.x < 15 && PlayerPosition.y < 12 ||
		// 1個目の2段目の地面から落ちてしまった場合（右）
		17 < PlayerPosition.x && PlayerPosition.x < 18 && PlayerPosition.y < 6 ||
		// 2個目の4段目の地面から落ちてしまった場合（左）
		24 < PlayerPosition.x && PlayerPosition.x < 25 && PlayerPosition.y < 12 ||
		// 2個目の4段目の地面から落ちてしまった場合（右）
		35 < PlayerPosition.x && PlayerPosition.x < 36 && PlayerPosition.y < 12 ||
		// 2個目の2段目の地面から落ちてしまった場合（右）
		38 < PlayerPosition.x && PlayerPosition.x < 39 && PlayerPosition.y < 6 ||
		// 3個目の4段目の地面から落ちてしまった場合（左）
		48 < PlayerPosition.x && PlayerPosition.x < 49 && PlayerPosition.y < 12 ||
		// 3個目の4段目の地面から落ちてしまった場合（右）
		59 < PlayerPosition.x && PlayerPosition.x < 60 && PlayerPosition.y < 12 ||
		// 3個目の2段目の地面から落ちてしまった場合（右）
		62 < PlayerPosition.x && PlayerPosition.x < 63 && PlayerPosition.y < 6 ||
		// 4個目の4段目の地面から落ちてしまった場合（左）
		75 < PlayerPosition.x && PlayerPosition.x < 76 && PlayerPosition.y < 12 ||
		// 4個目の4段目の地面から落ちてしまった場合（右）
		80 < PlayerPosition.x && PlayerPosition.x < 81 && PlayerPosition.y < 12) {
		jumpCount = 3;
	}

	// ジャンプ不可地帯にいる場合
	if (82 < PlayerPosition.x && PlayerPosition.x < 86 && 0 < PlayerPosition.y) {
		if (speed == 0.2f) {
			jumpPower = 0.35f;
		}
		else if (speed == 0.1f) {
			jumpPower = 0.3f;
		}
	}

	// 灰色ブロックの当たり判定
	if (77.8f < PlayerPosition.x && attackFlag == 2) {
		if (PlayerPosition.y < 12) { stageTwoMap->SetBlockFlag1(); }
		if (PlayerPosition.y < 9) { stageTwoMap->SetBlockFlag2(); }
		if (PlayerPosition.y < 6) { stageTwoMap->SetBlockFlag3(); }
		if (PlayerPosition.y < 3) { stageTwoMap->SetBlockFlag4(); }
	}

	// 看板の当たり判定
	if (88 < PlayerPosition.x && PlayerPosition.x < 92 && PlayerPosition.y == 0) { stageTwoMap->SetOperationFlagTrue(); }
	else { stageTwoMap->SetOperationFlagFalse(); }
}

// 通常ブロックの着地
void StageTwoPlayer::Land()
{
	if (jumpCount == 2) {
		PlayerRotation.z = 0.0f;
	}
	if (attackFlag == 2) {
		PlayerRotation.y = 0.0f;
		PlayerScale = { 1.9f, 0.1f, 1.9f };
		attackFlag = 0;
	}
	jumpPower = 0.0f;
	jumpCount = 0;
}

// 壊れるブロックの着地
void StageTwoPlayer::CrushBlockLand()
{
	if (jumpCount == 2) {
		PlayerRotation.z = 0.0f;
	}
	jumpPower = 0.0f;
	jumpCount = 0;
}

// エフェクト
void StageTwoPlayer::Effect()
{
	if (deathFlag == 1 && damageCount == 0 && EffectPosition[0].y < effectRange) {
		EffectPosition[0].y += 0.35f;
		EffectPosition[1].x += 0.35f / 1.41421356f;
		EffectPosition[1].y += 0.35f / 1.41421356f;
		EffectPosition[2].x += 0.35f;
		EffectPosition[3].x += 0.35f / 1.41421356f;
		EffectPosition[3].y -= 0.35f / 1.41421356f;
		EffectPosition[4].y -= 0.35f;
		EffectPosition[5].x -= 0.35f / 1.41421356f;
		EffectPosition[5].y -= 0.35f / 1.41421356f;
		EffectPosition[6].x -= 0.35f;
		EffectPosition[7].x -= 0.35f / 1.41421356f;
		EffectPosition[7].y += 0.35f / 1.41421356f;
	}
	effectRange = PlayerPosition.y + 4.5f;
}

// エフェクトの設定
void StageTwoPlayer::SetEffect()
{
	for (int i = 0; i < 8; i++) {
		EffectPosition[i].x = PlayerPosition.x;
		EffectPosition[i].y = PlayerPosition.y;
		EffectPosition[i].z = -3.0f;
	}
}

// リセット
void StageTwoPlayer::Reset()
{
	direction = 0;
	speed = 0.1f;
	jumpPower = 0;
	jumpCount = 0;
	damageCount = 0;
	attackFlag = 0;
	freezeFlag = 0;
	deathFlag = 0;
	CameraPosition = { 21, 8.3f, -20 };
	PlayerPosition = { 0,12,0 };
	PlayerRotation = { 0,0,0 };
	PlayerScale = { 1,1,1 };
	// エフェクト配置
	for (int i = 0; i < 8; i++) {
		EffectPosition[i] = { -10.0f, -10.0f, 0 };
	}
	stageTwoMap->Reset();
}