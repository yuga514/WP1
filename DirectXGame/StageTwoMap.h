#pragma once

#include "Object3d.h"
#include "SafeDelete.h"

using namespace DirectX;

class StageTwoMap
{
public: // メンバ関数
	// コンストラクタ
	StageTwoMap();

	// デストラクタ
	~StageTwoMap();

	// 初期化
	void Initialize();

	// 更新
	void Update();

	// 描画
	void Draw();

	// ゲッター
	void Getter();

	// セッター
	void Setter();

	// エフェクト
	void Effect();

	// リセット
	void Reset();

	// ブロックフラグの取得
	bool GetBlockFlag1() { return blockFlag[0]; }
	bool GetBlockFlag2() { return blockFlag[1]; }
	bool GetBlockFlag3() { return blockFlag[2]; }
	bool GetBlockFlag4() { return blockFlag[3]; }

	// ブロックフラグの設定
	void SetBlockFlag1() { blockFlag[0] = 1; }
	void SetBlockFlag2() { blockFlag[1] = 1; }
	void SetBlockFlag3() { blockFlag[2] = 1; }
	void SetBlockFlag4() { blockFlag[3] = 1; }

	// オペレーションフラグの設定(True)
	void SetOperationFlagTrue() { operationFlag = 1; }

	// オペレーションフラグの設定(False)
	void SetOperationFlagFalse() { operationFlag = 0; }

private: // メンバ変数
	const int mapX = 34;
	const int mapY = 9;

	XMFLOAT3 MapPosition[9][34] = {};

	int map[9][34] = {
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0},
		{1,1,1,1,1,1,0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,0,0,0,0,0,0,1,4,1,0,0,0,0},
		{1,1,1,1,1,1,0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,0,0,0,0,0,0,1,4,1,0,3,0,0},
		{1,1,1,1,1,1,1,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,0,1,4,1,0,0,0,0},
		{1,1,1,1,1,1,1,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,0,1,4,0,0,2,0,0},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
	};

	enum mapinfo
	{
		NONE,
		BLOCK,
		DOOR,
		OPERATION,
		GRAY
	};

	bool blockFlag[4] = {};
	bool operationFlag = 0;

	XMFLOAT3 EffectPosition[32] = {};
	float effectRange[4] = { 13.5f, 10.5f, 7.5f, 4.5f };

	Model* modelBlock = nullptr;
	Model* modelDoor = nullptr;
	Model* modelOperation = nullptr;
	Model* modelGrayBlock = nullptr;
	Model* modelGrayBall = nullptr;

	Object3d* objBlock[9][34] = {};
	Object3d* objDoor[9][34] = {};
	Object3d* objOperation[9][34] = {};
	Object3d* objGrayBlock[9][34] = {};
	Object3d* objEffect[32] = {};
};