#include "StageThreeMap.h"

// コンストラクタ
StageThreeMap::StageThreeMap()
{
}

// デストラクタ
StageThreeMap::~StageThreeMap()
{
	safe_delete(modelBlock);
	safe_delete(modelDoor);
	safe_delete(modelOperation);

	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				safe_delete(objBlock[y][x]);
			}
			if (map[y][x] == DOOR)
			{
				safe_delete(objDoor[y][x]);
			}
			if (map[y][x] == OPERATION)
			{
				safe_delete(objOperation[y][x]);
			}
		}
	}
}

// 初期化
void StageThreeMap::Initialize()
{
	// モデル読み込み
	modelBlock = Model::CreateFromOBJ("block");
	modelDoor = Model::CreateFromOBJ("door");
	modelOperation = Model::CreateFromOBJ("operation");

	// 3Dオブジェクト生成
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x] = Object3d::Create(modelBlock);
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x] = Object3d::Create(modelDoor);
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x] = Object3d::Create(modelOperation);
			}
		}
	}

	// マップチップ配置
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			MapPosition[y][x].x = 3.0f * x - 3.0f;
			MapPosition[y][x].y = -3.0f * y + 19.8f;
		}
	}
}

// 更新
void StageThreeMap::Update()
{
	// ゲッター
	Getter();

	// セッター
	Setter();

	// アップデート
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->Update();
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->Update();
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->Update();
			}
		}
	}
}

// 描画
void StageThreeMap::Draw()
{
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->Draw();
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->Draw();
			}
			if (map[y][x] == OPERATION)
			{
				if (operationFlag == 1) { objOperation[5][14]->Draw(); }
			}
		}
	}
}

// ゲッター
void StageThreeMap::Getter()
{
	// ゲットポジション
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->GetPosition();
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->GetPosition();
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->GetPosition();
			}
		}
	}

	// ゲットスケール
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->GetScale();
			}
		}
	}
}

// セッター
void StageThreeMap::Setter()
{
	// セットポジション
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BLOCK)
			{
				objBlock[y][x]->SetPosition(MapPosition[y][x]);
			}
			if (map[y][x] == DOOR)
			{
				objDoor[y][x]->SetPosition(MapPosition[y][x]);
			}
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->SetPosition(MapPosition[y][x]);
			}
		}
	}

	// セットスケール
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == OPERATION)
			{
				objOperation[y][x]->SetScale({ 0.5f, 0.5f, 0.5f });
			}
		}
	}
}