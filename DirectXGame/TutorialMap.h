#pragma once

#include "Object3d.h"
#include "SafeDelete.h"

using namespace DirectX;

class TutorialMap
{
public: // メンバ関数
	// コンストラクタ
	TutorialMap();

	// デストラクタ
	~TutorialMap();

	// 初期化
	void Initialize();

	// 更新
	void Update();

	// 描画
	void Draw();

	// ゲッター
	void Getter();

	// セッター
	void Setter();

	// エフェクト
	void Effect();

	// リセット
	void Reset();

	// ブロックフラグの取得
	bool GetBlockFlag1() { return blockFlag[0]; }
	bool GetBlockFlag2() { return blockFlag[1]; }
	bool GetBlockFlag3() { return blockFlag[2]; }
	bool GetBlockFlag4() { return blockFlag[3]; }
	bool GetBlockFlag5() { return blockFlag[4]; }

	// ブロックフラグの設定
	void SetBlockFlag1() { blockFlag[0] = 1; }
	void SetBlockFlag2() { blockFlag[1] = 1; }
	void SetBlockFlag3() { blockFlag[2] = 1; }
	void SetBlockFlag4() { blockFlag[3] = 1; }
	void SetBlockFlag5() { blockFlag[4] = 1; }

	// オペレーションフラグの設定(True)
	void SetOperationFlagTrue1() { operationFlag[0] = 1; }
	void SetOperationFlagTrue2() { operationFlag[1] = 1; }
	void SetOperationFlagTrue3() { operationFlag[2] = 1; }
	void SetOperationFlagTrue4() { operationFlag[3] = 1; }
	void SetOperationFlagTrue5() { operationFlag[4] = 1; }
	void SetOperationFlagTrue6() { operationFlag[5] = 1; }
	void SetOperationFlagTrue7() { operationFlag[6] = 1; }

	// オペレーションフラグの設定(False)
	void SetOperationFlagFalse1() { operationFlag[0] = 0; }
	void SetOperationFlagFalse2() { operationFlag[1] = 0; }
	void SetOperationFlagFalse3() { operationFlag[2] = 0; }
	void SetOperationFlagFalse4() { operationFlag[3] = 0; }
	void SetOperationFlagFalse5() { operationFlag[4] = 0; }
	void SetOperationFlagFalse6() { operationFlag[5] = 0; }
	void SetOperationFlagFalse7() { operationFlag[6] = 0; }

private: // メンバ変数
	const int mapX = 34;
	const int mapY = 9;

	XMFLOAT3 MapPosition[9][34] = {};

	int map[9][34] = {
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,1,0,0,0,3,0,0,1,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,4,0,0,0,6,0,0,1,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,5,1,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,5,1,0,0,0,0,0,3,0,0},
		{0,3,0,3,0,0,0,0,0,0,3,0,3,0,0,1,1,1,1,1,1,1,1,1,5,1,0,0,0,0,0,0,0,0},
		{0,6,0,6,0,0,0,0,0,0,6,0,6,0,1,1,1,1,1,1,1,1,1,1,5,0,0,0,0,0,0,2,0,0},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
	};

	enum mapinfo
	{
		NONE,
		BLOCK,
		DOOR,
		OPERATION,
		BROWN,
		GRAY,
		SIGN
	};

	bool operationFlag[7] = {};
	bool blockFlag[5] = {};

	XMFLOAT3 EffectPosition[40] = {};
	float effectRange[5] = { 16.5f, 13.5f, 10.5f, 7.5f, 4.5f };

	Model* modelBlock = nullptr;
	Model* modelDoor = nullptr;
	Model* modelOperation = nullptr;
	Model* modelBrownBlock = nullptr;
	Model* modelGrayBlock = nullptr;
	Model* modelSign = nullptr;
	Model* modelBrownBall = nullptr;
	Model* modelGrayBall = nullptr;

	Object3d* objBlock[9][34] = {};
	Object3d* objDoor[9][34] = {};
	Object3d* objOperation[9][34] = {};
	Object3d* objBrownBlock[9][34] = {};
	Object3d* objGrayBlock[9][34] = {};
	Object3d* objSign[9][34] = {};
	Object3d* objEffect[40] = {};
};