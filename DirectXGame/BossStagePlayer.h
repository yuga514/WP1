#pragma once

#include "Object3d.h"
#include "SafeDelete.h"
#include "Input.h"
#include "DebugCamera.h"
#include "WinApp.h"
#include "SimpleMap.h"

using namespace DirectX;

class BossStagePlayer
{
public: // メンバ関数
	// コンストラクタ
	BossStagePlayer();

	// デストラクタ
	~BossStagePlayer();

	// 初期化
	void Initialize(Input* input);

	// 更新
	void Update();

	// 描画
	void Draw();

	// ゲッター
	void Getter();

	// セッター
	void Setter();

	// アクション
	void Action();

	// 当たり判定
	void Collision();

	// 通常ブロックの着地
	void Land();

	// エフェクト
	void Effect();

	// リセット
	void Reset();

	// アタックフラグの取得
	unsigned int GetAttackFlag() { return attackFlag; }

	// ダメージカウントの取得
	unsigned int GetDamageCount() { return damageCount; }

	// デスフラグの取得
	bool GetDeathFlag() { return deathFlag; }

	// エフェクトのY座標の取得
	float GetEffectPositionY() { return EffectPosition[0].y; }

	// エフェクトの範囲の取得
	float GetEffectRange() { return effectRange; }

	// 座標の取得
	XMFLOAT3 GetPosition() { return objPlayerRight->GetPosition(); }

	// 注視点座標の取得
	XMFLOAT3 GetTarget() { return camera->GetTarget(); }

	// ダメージカウントの設定
	void SetDamageCount() { damageCount = 120; }

	// フリーズフラグの設定(True)
	void SetFreezeFlagTrue() { freezeFlag = 1; }

	// フリーズフラグの設定(False)
	void SetFreezeFlagFalse() { freezeFlag = 0; }

	// デスフラグの設定
	void SetDeathFlag() { deathFlag = 1; }

	// エフェクトの設定
	void SetEffect();

private: // メンバ変数
	bool direction = 0;
	float speed = 0.1f;
	float jumpPower = 0;
	unsigned int jumpCount = 0;
	unsigned int damageCount = 0;
	unsigned int attackFlag = 0;
	bool freezeFlag = 0;
	bool deathFlag = 0;

	XMFLOAT3 CameraPosition = { 21, 8.3f, -20 };
	XMFLOAT3 PlayerPosition = { 0,0,0 };
	XMFLOAT3 PlayerRotation = { 0,0,0 };
	XMFLOAT3 PlayerScale = { 1,1,1 };

	XMFLOAT3 EffectPosition[8] = {};
	float effectRange = 0.0f;

	Input* input = nullptr;
	DebugCamera* camera = nullptr;

	Model* modelPlayerRight = nullptr;
	Model* modelPlayerLeft = nullptr;
	Model* modelAttack1 = nullptr;
	Model* modelAttack2 = nullptr;
	Model* modelYellowBall = nullptr;

	Object3d* objPlayerRight = nullptr;
	Object3d* objPlayerLeft = nullptr;
	Object3d* objAttack1 = nullptr;
	Object3d* objAttack2 = nullptr;
	Object3d* objEffect[8] = {};

	SimpleMap* simpleMap = nullptr;
};