#pragma once

#include "Object3d.h"
#include "SafeDelete.h"

using namespace DirectX;

class StageThreeMap
{
public: // メンバ関数
	// コンストラクタ
	StageThreeMap();

	// デストラクタ
	~StageThreeMap();

	// 初期化
	void Initialize();

	// 更新
	void Update();

	// 描画
	void Draw();

	// ゲッター
	void Getter();

	// セッター
	void Setter();

	// オペレーションフラグの設定(True)
	void SetOperationFlagTrue() { operationFlag = 1; }

	// オペレーションフラグの設定(False)
	void SetOperationFlagFalse() { operationFlag = 0; }

private: // メンバ変数
	const int mapX = 17;
	const int mapY = 9;

	XMFLOAT3 MapPosition[9][17] = {};

	int map[9][17] = {
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
	};

	enum mapinfo
	{
		NONE,
		BLOCK,
		DOOR,
		OPERATION,
	};

	bool operationFlag = 0;

	Model* modelBlock = nullptr;
	Model* modelDoor = nullptr;
	Model* modelOperation = nullptr;

	Object3d* objBlock[9][17] = {};
	Object3d* objDoor[9][17] = {};
	Object3d* objOperation[9][17] = {};
};