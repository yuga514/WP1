#include "BossStagePlayer.h"

// コンストラクタ
BossStagePlayer::BossStagePlayer()
{
}

// デストラクタ
BossStagePlayer::~BossStagePlayer()
{
	safe_delete(modelPlayerRight);
	safe_delete(modelPlayerLeft);
	safe_delete(modelAttack1);
	safe_delete(modelAttack2);
	safe_delete(modelYellowBall);
	safe_delete(objPlayerRight);
	safe_delete(objPlayerLeft);
	safe_delete(objAttack1);
	safe_delete(objAttack2);
	for (int i = 0; i < 8; i++) {
		safe_delete(objEffect[i]);
	}
	safe_delete(simpleMap);
}

// 初期化
void BossStagePlayer::Initialize(Input* input)
{
	this->input = input;

	// カメラ生成
	camera = new DebugCamera(WinApp::window_width, WinApp::window_height, input);

	// 3Dオブジェクトにカメラをセット
	Object3d::SetCamera(camera);

	// カメラ注視点をセット
	camera->SetDistance(3.0f);

	// モデル読み込み
	modelPlayerRight = Model::CreateFromOBJ("playerRight");
	modelPlayerLeft = Model::CreateFromOBJ("playerLeft");
	modelAttack1 = Model::CreateFromOBJ("attack1");
	modelAttack2 = Model::CreateFromOBJ("attack2");
	modelYellowBall = Model::CreateFromOBJ("yellowBall");

	// 3Dオブジェクト生成
	objPlayerRight = Object3d::Create(modelPlayerRight);
	objPlayerLeft = Object3d::Create(modelPlayerLeft);
	objAttack1 = Object3d::Create(modelAttack1);
	objAttack2 = Object3d::Create(modelAttack2);
	for (int i = 0; i < 8; i++) {
		objEffect[i] = Object3d::Create(modelYellowBall);
	}

	// エフェクト配置
	for (int i = 0; i < 8; i++) {
		EffectPosition[i] = { -10, -10, 0 };
	}

	// シンプルマップ生成
	simpleMap = new SimpleMap();
	simpleMap->Initialize();
}

// 更新
void BossStagePlayer::Update()
{
	// ゲッター
	Getter();

	// アクション
	if (freezeFlag == 0 && deathFlag == 0) { Action(); }
	if (deathFlag == 1 && 1 < damageCount) { PlayerPosition.y += 0.05f; }

	// ダメージを受けたら
	if (0 < damageCount) { damageCount--; }

	// 当たり判定
	Collision();

	// エフェクト
	Effect();

	// セッター
	Setter();

	// アップデート
	camera->Update();

	objPlayerRight->Update();
	objPlayerLeft->Update();
	objAttack1->Update();
	objAttack2->Update();
	for (int i = 0; i < 8; i++) {
		objEffect[i]->Update();
	}
	simpleMap->Update();
}

// 描画
void BossStagePlayer::Draw()
{
	if ((deathFlag == 0 || deathFlag == 1 && damageCount != 0) && damageCount % 2 == 0) {
		if (direction == 0) {
			objPlayerRight->Draw();
		}
		if (direction == 1) {
			objPlayerLeft->Draw();
		}
		if (attackFlag == 1) {
			objAttack1->Draw();
		}
		if (attackFlag == 2) {
			objAttack2->Draw();
		}
	}
	for (int i = 0; i < 8; i++) {
		if (deathFlag == 1 && EffectPosition[0].y < effectRange) {
			objEffect[i]->Draw();
		}
	}
	simpleMap->Draw();
}

// ゲッター
void BossStagePlayer::Getter()
{
	// ゲットターゲット
	camera->GetTarget();

	// ゲットポジション
	objPlayerRight->GetPosition();
	objPlayerLeft->GetPosition();
	objAttack1->GetPosition();
	objAttack2->GetPosition();
	for (int i = 0; i < 8; i++) {
		objEffect[i]->GetPosition();
	}

	// ゲットローテーション
	objPlayerRight->GetRotation();
	objPlayerLeft->GetRotation();

	// ゲットスケール
	objPlayerRight->GetScale();
	objPlayerLeft->GetScale();
}

// セッター
void BossStagePlayer::Setter()
{
	// セットターゲット
	camera->SetTarget(CameraPosition);

	// セットポジション
	objPlayerRight->SetPosition(PlayerPosition);
	objPlayerLeft->SetPosition(PlayerPosition);
	objAttack1->SetPosition(PlayerPosition);
	objAttack2->SetPosition(PlayerPosition);
	for (int i = 0; i < 8; i++) {
		objEffect[i]->SetPosition(EffectPosition[i]);
	}

	// セットローテーション
	objPlayerRight->SetRotation(PlayerRotation);
	objPlayerLeft->SetRotation(PlayerRotation);

	// セットスケール
	objPlayerRight->SetScale(PlayerScale);
	objPlayerLeft->SetScale(PlayerScale);
}

// アクション
void BossStagePlayer::Action()
{
	// スティックの入力を取得
	Input::StickMove stickMove = input->GetStickMove();

	// 落下処理
	PlayerPosition.y -= jumpPower;

	// 落下加速度を加える
	jumpPower += 0.01f;

	// 右移動
	if (input->PushKey(DIK_RIGHT) || 42767 < stickMove.lX && input->GetDevJoyStick()) {
		PlayerPosition.x += speed;
		PlayerRotation.z -= 3.6f;
		direction = 0;
	}

	// 左移動
	if (input->PushKey(DIK_LEFT) || stickMove.lX < 22767 && input->GetDevJoyStick()) {
		PlayerPosition.x -= speed;
		PlayerRotation.z += 3.6f;
		direction = 1;
	}

	// ダッシュ
	if (jumpCount == 0) {
		if (input->PushKey(DIK_LSHIFT) || input->PushButton(4)) {
			speed = 0.2f;
		}
		else if (input->PushKey(DIK_LSHIFT) == 0 || input->PushButton(4) == 0) {
			speed = 0.1f;
		}
	}

	// 0段目の地面についていたら止まる
	if (PlayerPosition.y < 0) {
		PlayerPosition.y = 0.0f;
		Land();
	}

	// ジャンプ
	if ((input->TriggerKey(DIK_Z) || input->TriggerButton(0)) && jumpCount < 2) {
		if (speed == 0.2f) {
			jumpPower = -0.35f;
		}
		else if (speed == 0.1f) {
			jumpPower = -0.3f;
		}
		jumpCount += 1;
	}
	if (jumpCount == 2) {
		if (direction == 0) {
			PlayerRotation.z -= 36.0f;
		}
		else if (direction == 1) {
			PlayerRotation.z += 36.0f;
		}
	}

	// 攻撃
	if ((input->TriggerKey(DIK_X) || input->TriggerButton(1)) && attackFlag == 0) {
		if ((input->PushKey(DIK_DOWN) || 42767 < stickMove.lY && input->GetDevJoyStick()) && jumpCount == 2) {
			attackFlag = 2;
		}
		else {
			attackFlag = 1;
		}
	}
	if (attackFlag == 2) {
		PlayerRotation.y += 36.0f;
		PlayerPosition.y -= 1.0f;
		PlayerRotation.z = 0.0f;
	}
	else if (attackFlag == 1) {
		PlayerRotation.y += 36.0f;
		PlayerRotation.z = 0.0f;
		if (PlayerRotation.y == 720) {
			PlayerRotation.y = 0.0f;
			attackFlag = 0;
		}
	}

	// プレイヤーが凹んでいたら
	if (PlayerScale.y < 1) {
		PlayerScale.x -= 0.1f;
		PlayerScale.y += 0.1f;
		PlayerScale.z -= 0.1f;
	}
}

// 当たり判定
void BossStagePlayer::Collision()
{
	// 画面外に出ないようにする
	if (PlayerPosition.x < 0) {
		PlayerPosition.x = 0.0f;
	}
	if (42 < PlayerPosition.x) {
		PlayerPosition.x = 42.0f;
	}
}

// 通常ブロックの着地
void BossStagePlayer::Land()
{
	if (jumpCount == 2) {
		PlayerRotation.z = 0.0f;
	}
	if (attackFlag == 2) {
		PlayerRotation.y = 0.0f;
		PlayerScale = { 1.9f, 0.1f, 1.9f };
		attackFlag = 0;
	}
	jumpPower = 0.0f;
	jumpCount = 0;
}

// エフェクト
void BossStagePlayer::Effect()
{
	if (deathFlag == 1 && damageCount == 0 && EffectPosition[0].y < effectRange) {
		EffectPosition[0].y += 0.35f;
		EffectPosition[1].x += 0.35f / 1.41421356f;
		EffectPosition[1].y += 0.35f / 1.41421356f;
		EffectPosition[2].x += 0.35f;
		EffectPosition[3].x += 0.35f / 1.41421356f;
		EffectPosition[3].y -= 0.35f / 1.41421356f;
		EffectPosition[4].y -= 0.35f;
		EffectPosition[5].x -= 0.35f / 1.41421356f;
		EffectPosition[5].y -= 0.35f / 1.41421356f;
		EffectPosition[6].x -= 0.35f;
		EffectPosition[7].x -= 0.35f / 1.41421356f;
		EffectPosition[7].y += 0.35f / 1.41421356f;
	}
	effectRange = PlayerPosition.y + 4.5f;
}

// エフェクトの設定
void BossStagePlayer::SetEffect()
{
	for (int i = 0; i < 8; i++) {
		EffectPosition[i].x = PlayerPosition.x;
		EffectPosition[i].y = PlayerPosition.y;
		EffectPosition[i].z = -3.0f;
	}
}

// リセット
void BossStagePlayer::Reset()
{
	direction = 0;
	speed = 0.1f;
	jumpPower = 0;
	jumpCount = 0;
	damageCount = 0;
	attackFlag = 0;
	freezeFlag = 0;
	deathFlag = 0;
	CameraPosition = { 21, 8.3f, -20 };
	PlayerPosition = { 0,0,0 };
	PlayerRotation = { 0,0,0 };
	PlayerScale = { 1,1,1 };
	// エフェクト配置
	for (int i = 0; i < 8; i++) {
		EffectPosition[i] = { -10, -10, 0 };
	}
}