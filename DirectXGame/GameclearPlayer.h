#pragma once

#include "Object3d.h"
#include "SafeDelete.h"
#include "Input.h"
#include "DebugCamera.h"
#include "WinApp.h"
#include "SimpleMap.h"

using namespace DirectX;

class GameclearPlayer
{
public: // メンバ関数
	// コンストラクタ
	GameclearPlayer();

	// デストラクタ
	~GameclearPlayer();

	// 初期化
	void Initialize(Input* input);

	// 更新
	void Update();

	// 描画
	void Draw();

	// ゲッター
	void Getter();

	// セッター
	void Setter();

	// アクション
	void Action();

	// リセット
	void Reset();

	// 座標の取得
	XMFLOAT3 GetPosition() { return objPlayerRight->GetPosition(); }

	// 注視点座標の取得
	XMFLOAT3 GetTarget() { return camera->GetTarget(); }

private: // メンバ変数
	float jumpPower = 0;
	bool landFlag = 0;
	bool moveFlag = 0;

	XMFLOAT3 CameraPosition = { 21, 8.3f, -20 };
	XMFLOAT3 PlayerPosition = { 21,0,0 };
	XMFLOAT3 PlayerRotation = { 0,90,0 };

	Input* input = nullptr;
	DebugCamera* camera = nullptr;

	Model* modelPlayerRight = nullptr;

	Object3d* objPlayerRight = nullptr;

	SimpleMap* simpleMap = nullptr;
};