#include "SimpleMap.h"

// コンストラクタ
SimpleMap::SimpleMap()
{
}

// デストラクタ
SimpleMap::~SimpleMap()
{
	safe_delete(modelBrock);

	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BROCK)
			{
				safe_delete(objBrock[y][x]);
			}
		}
	}
}

// 初期化
void SimpleMap::Initialize()
{
	// モデル読み込み
	modelBrock = Model::CreateFromOBJ("block");

	// 3Dオブジェクト生成
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BROCK)
			{
				objBrock[y][x] = Object3d::Create(modelBrock);
			}
		}
	}

	// マップチップ配置
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			MapPosition[y][x].x = 3.0f * x - 3.0f;
			MapPosition[y][x].y = -3.0f * y + 19.8f;
		}
	}
}

// 更新
void SimpleMap::Update()
{
	// ゲッター
	Getter();

	// セッター
	Setter();

	// アップデート
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BROCK)
			{
				objBrock[y][x]->Update();
			}
		}
	}
}

// 描画
void SimpleMap::Draw()
{
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BROCK)
			{
				objBrock[y][x]->Draw();
			}
		}
	}
}

// ゲッター
void SimpleMap::Getter()
{
	// ゲットポジション
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BROCK)
			{
				objBrock[y][x]->GetPosition();
			}
		}
	}
}

// セッター
void SimpleMap::Setter()
{
	// セットポジション
	for (int y = 0; y < mapY; y++) {
		for (int x = 0; x < mapX; x++) {
			if (map[y][x] == BROCK)
			{
				objBrock[y][x]->SetPosition(MapPosition[y][x]);
			}
		}
	}
}