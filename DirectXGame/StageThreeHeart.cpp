#include "StageThreeHeart.h"

// コンストラクタ
StageThreeHeart::StageThreeHeart()
{
}

// デストラクタ
StageThreeHeart::~StageThreeHeart()
{
	safe_delete(modelHeart);
	safe_delete(modelRhombus);
	safe_delete(objHeart);
	for (int i = 0; i < 8; i++) {
		safe_delete(objEffect[i]);
	}
}

// 初期化
void StageThreeHeart::Initialize()
{
	// モデル読み込み
	modelHeart = Model::CreateFromOBJ("heart");
	modelRhombus = Model::CreateFromOBJ("rhombus");

	// 3Dオブジェクト生成
	objHeart = Object3d::Create(modelHeart);
	for (int i = 0; i < 8; i++) {
		objEffect[i] = Object3d::Create(modelRhombus);
	}

	// エフェクト配置
	for (int i = 0; i < 8; i++) {
		EffectPosition[i] = { 9, 0, -3 };
	}
}

// 更新
void StageThreeHeart::Update()
{
	// ゲッター
	Getter();

	// アクション
	if (freezeFlag == 0 && heartFlag == 0) { Action(); }

	// エフェクト
	Effect();

	// セッター
	Setter();

	// アップデート
	objHeart->Update();
	for (int i = 0; i < 8; i++) {
		objEffect[i]->Update();
	}
}

// 描画
void StageThreeHeart::Draw()
{
	if (heartFlag == 0)
	{
		objHeart->Draw();
	}
	for (int i = 0; i < 8; i++) {
		if (heartFlag == 1 && EffectPosition[0].y < effectRange) {
			objEffect[i]->Draw();
		}
	}
}

// ゲッター
void StageThreeHeart::Getter()
{
	// ゲットポジション
	objHeart->GetPosition();
	for (int i = 0; i < 8; i++) {
		objEffect[i]->GetPosition();
	}

	// ゲットローテーション
	objHeart->GetRotation();

	// ゲットスケール
	objHeart->GetScale();
}

// セッター
void StageThreeHeart::Setter()
{
	// セットポジション
	objHeart->SetPosition(HeartPosition);
	for (int i = 0; i < 8; i++) {
		objEffect[i]->SetPosition(EffectPosition[i]);
	}

	// セットローテーション
	objHeart->SetRotation(HeartRotation);

	// セットスケール
	objHeart->SetScale({ 0.5f,0.5f,0.5f });
}

// アクション
void StageThreeHeart::Action()
{
	// 回転
	HeartRotation.y += 2.0f;
	if (HeartRotation.y == 360) { HeartRotation.y = 0.0f; }
}

// エフェクト
void StageThreeHeart::Effect()
{
	if (heartFlag == 1 && EffectPosition[0].y < effectRange) {
		EffectPosition[0].y += 0.175f;
		EffectPosition[1].x += 0.175f / 1.41421356f;
		EffectPosition[1].y += 0.175f / 1.41421356f;
		EffectPosition[2].x += 0.175f;
		EffectPosition[3].x += 0.175f / 1.41421356f;
		EffectPosition[3].y -= 0.175f / 1.41421356f;
		EffectPosition[4].y -= 0.175f;
		EffectPosition[5].x -= 0.175f / 1.41421356f;
		EffectPosition[5].y -= 0.175f / 1.41421356f;
		EffectPosition[6].x -= 0.175f;
		EffectPosition[7].x -= 0.175f / 1.41421356f;
		EffectPosition[7].y += 0.175f / 1.41421356f;
	}
}

// リセット
void StageThreeHeart::Reset()
{
	freezeFlag = 0;
	heartFlag = 0;
	HeartPosition = { 9,0,0 };
	HeartRotation = { 0,0,0 };
	// エフェクト配置
	for (int i = 0; i < 8; i++) {
		EffectPosition[i] = { 9, 0, -3 };
	}
}