#include "StageTwoEnemy.h"

// コンストラクタ
StageTwoEnemy::StageTwoEnemy()
{
}

// デストラクタ
StageTwoEnemy::~StageTwoEnemy()
{
	safe_delete(modelBrownEnemy);
	safe_delete(modelGrayEnemy);
	safe_delete(modelYellowBall);

	for (int i = 0; i < 3; i++) {
		safe_delete(objEnemy[i]);
	}
	for (int i = 0; i < 24; i++) {
		safe_delete(objEffect[i]);
	}
}

// 初期化
void StageTwoEnemy::Initialize()
{
	// モデル読み込み
	modelBrownEnemy = Model::CreateFromOBJ("brownEnemy");
	modelGrayEnemy = Model::CreateFromOBJ("grayEnemy");
	modelYellowBall = Model::CreateFromOBJ("yellowBall");

	// 3Dオブジェクト生成
	objEnemy[0] = Object3d::Create(modelBrownEnemy);
	objEnemy[1] = Object3d::Create(modelBrownEnemy);
	objEnemy[2] = Object3d::Create(modelGrayEnemy);
	for (int i = 0; i < 24; i++) {
		objEffect[i] = Object3d::Create(modelYellowBall);
	}

	// エフェクト配置
	for (int i = 0; i < 24; i++) {
		EffectPosition[i] = { -10.0f, -10.0f, 0 };
	}
}

// 更新
void StageTwoEnemy::Update()
{
	// ゲッター
	Getter();

	// アクション
	if (freezeFlag == 0) { Action(); }

	// エフェクト
	Effect();

	// セッター
	Setter();

	// アップデート
	for (int i = 0; i < 3; i++) {
		objEnemy[i]->Update();
	}
	for (int i = 0; i < 24; i++) {
		objEffect[i]->Update();
	}
}

// 描画
void StageTwoEnemy::Draw()
{
	for (int i = 0; i < 3; i++) {
		if (deathFlag[i] == 0) {
			objEnemy[i]->Draw();
		}
	}
	for (int i = 0; i < 24; i++) {
		if (i < 8 && deathFlag[0] == 1 && EffectPosition[0].y < effectRange[0]) {
			objEffect[i]->Draw();
		}
		if (7 < i && i < 16 && deathFlag[1] == 1 && EffectPosition[8].y < effectRange[1]) {
			objEffect[i]->Draw();
		}
		if (15 < i && deathFlag[2] == 1 && EffectPosition[16].y < effectRange[2]) {
			objEffect[i]->Draw();
		}
	}
}

// ゲッター
void StageTwoEnemy::Getter()
{
	// ゲットポジション
	for (int i = 0; i < 3; i++) {
		objEnemy[i]->GetPosition();
	}
	for (int i = 0; i < 24; i++) {
		objEffect[i]->GetPosition();
	}

	// ゲットローテーション
	for (int i = 0; i < 3; i++) {
		objEnemy[i]->GetRotation();
	}
}

// セッター
void StageTwoEnemy::Setter()
{
	// セットポジション
	for (int i = 0; i < 3; i++) {
		objEnemy[i]->SetPosition(EnemyPosition[i]);
	}
	for (int i = 0; i < 24; i++) {
		objEffect[i]->SetPosition(EffectPosition[i]);
	}

	// セットローテーション
	for (int i = 0; i < 3; i++) {
		objEnemy[i]->SetRotation(EnemyRotation[i]);
	}
}

// アクション
void StageTwoEnemy::Action()
{
	for (int i = 0; i < 3; i++) {
		// 生きているとき
		if (deathFlag[i] == 0) {
			// 左移動
			if (moveFlag[i] == 0) {
				EnemyPosition[i].x -= 0.1f;
			}

			// 右移動
			if (moveFlag[i] == 1) {
				EnemyPosition[i].x += 0.1f;
			}

			// 回転
			EnemyRotation[i].y += 3.6f;
			if (EnemyRotation[i].y == 360) {
				EnemyRotation[i].y = 0.0f;
			}
		}
	}
	// 方向転換
	if (EnemyPosition[0].x < 18) { moveFlag[0] = 1; }
	if (EnemyPosition[1].x < 39) { moveFlag[1] = 1; }
	if (EnemyPosition[2].x < 63) { moveFlag[2] = 1; }
	if (24 < EnemyPosition[0].x) { moveFlag[0] = 0; }
	if (48 < EnemyPosition[1].x) { moveFlag[1] = 0; }
	if (75 < EnemyPosition[2].x) { moveFlag[2] = 0; }
}

// エフェクト
void StageTwoEnemy::Effect()
{
	// 敵1
	if (deathFlag[0] == 1 && EffectPosition[0].y < effectRange[0]) {
		EffectPosition[0].y += 0.35f;
		EffectPosition[1].x += 0.35f / 1.41421356f;
		EffectPosition[1].y += 0.35f / 1.41421356f;
		EffectPosition[2].x += 0.35f;
		EffectPosition[3].x += 0.35f / 1.41421356f;
		EffectPosition[3].y -= 0.35f / 1.41421356f;
		EffectPosition[4].y -= 0.35f;
		EffectPosition[5].x -= 0.35f / 1.41421356f;
		EffectPosition[5].y -= 0.35f / 1.41421356f;
		EffectPosition[6].x -= 0.35f;
		EffectPosition[7].x -= 0.35f / 1.41421356f;
		EffectPosition[7].y += 0.35f / 1.41421356f;
	}
	// 敵2
	if (deathFlag[1] == 1 && EffectPosition[8].y < effectRange[1]) {
		EffectPosition[8].y += 0.35f;
		EffectPosition[9].x += 0.35f / 1.41421356f;
		EffectPosition[9].y += 0.35f / 1.41421356f;
		EffectPosition[10].x += 0.35f;
		EffectPosition[11].x += 0.35f / 1.41421356f;
		EffectPosition[11].y -= 0.35f / 1.41421356f;
		EffectPosition[12].y -= 0.35f;
		EffectPosition[13].x -= 0.35f / 1.41421356f;
		EffectPosition[13].y -= 0.35f / 1.41421356f;
		EffectPosition[14].x -= 0.35f;
		EffectPosition[15].x -= 0.35f / 1.41421356f;
		EffectPosition[15].y += 0.35f / 1.41421356f;
	}
	// 敵3
	if (deathFlag[2] == 1 && EffectPosition[16].y < effectRange[2]) {
		EffectPosition[16].y += 0.35f;
		EffectPosition[17].x += 0.35f / 1.41421356f;
		EffectPosition[17].y += 0.35f / 1.41421356f;
		EffectPosition[18].x += 0.35f;
		EffectPosition[19].x += 0.35f / 1.41421356f;
		EffectPosition[19].y -= 0.35f / 1.41421356f;
		EffectPosition[20].y -= 0.35f;
		EffectPosition[21].x -= 0.35f / 1.41421356f;
		EffectPosition[21].y -= 0.35f / 1.41421356f;
		EffectPosition[22].x -= 0.35f;
		EffectPosition[23].x -= 0.35f / 1.41421356f;
		EffectPosition[23].y += 0.35f / 1.41421356f;
	}
}

// エフェクトの設定
void StageTwoEnemy::SetEffect1()
{
	for (int i = 0; i < 8; i++) {
		EffectPosition[i].x = EnemyPosition[0].x;
		EffectPosition[i].y = EnemyPosition[0].y;
		EffectPosition[i].z = -3.0f;
	}
}

void StageTwoEnemy::SetEffect2()
{
	for (int i = 8; i < 16; i++) {
		EffectPosition[i].x = EnemyPosition[1].x;
		EffectPosition[i].y = EnemyPosition[1].y;
		EffectPosition[i].z = -3.0f;
	}
}

void StageTwoEnemy::SetEffect3()
{
	for (int i = 16; i < 24; i++) {
		EffectPosition[i].x = EnemyPosition[2].x;
		EffectPosition[i].y = EnemyPosition[2].y;
		EffectPosition[i].z = -3.0f;
	}
}

void StageTwoEnemy::Reset()
{
	freezeFlag = 0;
	for (int i = 0; i < 3; i++) {
		moveFlag[i] = 0;
		deathFlag[i] = 0;
	}
	EnemyPosition[0] = { 21, 0, 0 };
	EnemyPosition[1] = { 45, 0, 0 };
	EnemyPosition[2] = { 72, 0, 0 };
	for (int i = 0; i < 3; i++) {
		EnemyRotation[i] = { 0,0,0 };
	}
	// エフェクト配置
	for (int i = 0; i < 24; i++) {
		EffectPosition[i] = { -10.0f, -10.0f, 0 };
	}
}