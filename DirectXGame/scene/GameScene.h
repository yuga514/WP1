﻿#pragma once

#include <DirectXMath.h>

#include "SafeDelete.h"
#include "DirectXCommon.h"
#include "Input.h"
#include "Sprite.h"
#include "Object3d.h"
#include "DebugText.h"
#include "Audio.h"
#include "DebugCamera.h"
#include "TitlePlayer.h"
#include "TutorialPlayer.h"
#include "StageOnePlayer.h"
#include "StageOneEnemy.h"
#include "StageTwoPlayer.h"
#include "StageTwoEnemy.h"
#include "StageThreePlayer.h"
#include "StageThreeHeart.h"
#include "BossStagePlayer.h"
#include "BossStageEnemy.h"
#include "GameclearPlayer.h"
#include "GameoverPlayer.h"

// ゲームシーン
class GameScene
{
private: // エイリアス
	// Microsoft::WRL::を省略
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	// DirectX::を省略
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;

private: // 静的メンバ変数
	static const int debugTextTexNumber = 0;

public: // メンバ関数
	// コンストクラタ
	GameScene();

	// デストラクタ
	~GameScene();

	// 初期化
	void Initialize(DirectXCommon* dxCommon, Input* input, Audio* audio);

	// 毎フレーム処理
	void Update();

	// 描画
	void Draw();

	// サウンド
	void Sound();

	// 当たり判定
	void Collision();

	// ポーズ
	void Pause();

	// シーン切り替え
	void SceneChange();

	// カメラのセット
	void SetCamera();

	// リセット
	void Reset();

	TutorialMap* tutorialMap = nullptr;

private: // メンバ変数
	DirectXCommon* dxCommon = nullptr;
	Input* input = nullptr;
	DebugCamera* camera = nullptr;
	Audio* audio = nullptr;
	DebugText debugText;

	// ゲームシーン用
	char str[100] = { 0 };
	unsigned int scene = 0;
	unsigned int HP = 5;
	unsigned int bossHP = 5;
	unsigned int pauseMenu = 0;
	bool endMenu = 0;
	bool pauseFlag = 0;
	bool freezeFlag = 0;
	bool signFlag[7] = {};
	bool sceneChangeFlag[6] = {};

	XMFLOAT2 BlackPosition[7] = { {-1380, 0}, {-1380, 0}, {-1380, 0}, {-1380, 0}, {-1380, 0}, {-1380, 0}, {0, 0} };
	XMFLOAT4 BlackColor = { 1,1,1,0 };
	XMFLOAT4 WhiteColor = { 1,1,1,0 };

	Sprite* title = nullptr;
	Sprite* HP5 = nullptr;
	Sprite* HP4 = nullptr;
	Sprite* HP3 = nullptr;
	Sprite* HP2 = nullptr;
	Sprite* HP1 = nullptr;
	Sprite* HP0 = nullptr;
	Sprite* bossHP5 = nullptr;
	Sprite* bossHP4 = nullptr;
	Sprite* bossHP3 = nullptr;
	Sprite* bossHP2 = nullptr;
	Sprite* bossHP1 = nullptr;
	Sprite* bossHP0 = nullptr;
	Sprite* background1 = nullptr;
	Sprite* background2 = nullptr;
	Sprite* background3 = nullptr;
	Sprite* menu1 = nullptr;
	Sprite* menu2 = nullptr;
	Sprite* menu3 = nullptr;
	Sprite* sign1 = nullptr;
	Sprite* sign2 = nullptr;
	Sprite* sign3 = nullptr;
	Sprite* sign4 = nullptr;
	Sprite* sign5 = nullptr;
	Sprite* sign6 = nullptr;
	Sprite* sign7 = nullptr;
	Sprite* gameclear = nullptr;
	Sprite* gameover1 = nullptr;
	Sprite* gameover2 = nullptr;
	Sprite* black[7] = {};
	Sprite* white = nullptr;

	TitlePlayer* titlePlayer = nullptr;
	TutorialPlayer* tutorialPlayer = nullptr;
	StageOnePlayer* stageOnePlayer = nullptr;
	StageOneEnemy* stageOneEnemy = nullptr;
	StageTwoPlayer* stageTwoPlayer = nullptr;
	StageTwoEnemy* stageTwoEnemy = nullptr;
	StageThreePlayer* stageThreePlayer = nullptr;
	StageThreeHeart* stageThreeHeart = nullptr;
	BossStagePlayer* bossStagePlayer = nullptr;
	BossStageEnemy* bossStageEnemy = nullptr;
	GameclearPlayer* gameclearPlayer = nullptr;
	GameoverPlayer* gameoverPlayer = nullptr;
};