﻿#include <cassert>

#include "GameScene.h"

using namespace DirectX;

GameScene::GameScene()
{
}

GameScene::~GameScene()
{
	safe_delete(title);
	safe_delete(HP5);
	safe_delete(HP4);
	safe_delete(HP3);
	safe_delete(HP2);
	safe_delete(HP1);
	safe_delete(HP0);
	safe_delete(bossHP5);
	safe_delete(bossHP4);
	safe_delete(bossHP3);
	safe_delete(bossHP2);
	safe_delete(bossHP1);
	safe_delete(bossHP0);
	safe_delete(background1);
	safe_delete(background2);
	safe_delete(background3);
	safe_delete(menu1);
	safe_delete(menu2);
	safe_delete(menu3);
	safe_delete(sign1);
	safe_delete(sign2);
	safe_delete(sign3);
	safe_delete(sign4);
	safe_delete(sign5);
	safe_delete(sign6);
	safe_delete(sign7);
	safe_delete(gameclear);
	safe_delete(gameover1);
	safe_delete(gameover2);
	for (int i = 0; i < 7; i++) {
		safe_delete(black[i]);
	}
	safe_delete(white);
	safe_delete(titlePlayer);
	safe_delete(tutorialPlayer);
	safe_delete(stageOnePlayer);
	safe_delete(stageOneEnemy);
	safe_delete(stageTwoPlayer);
	safe_delete(stageTwoEnemy);
	safe_delete(stageThreePlayer);
	safe_delete(stageThreeHeart);
	safe_delete(bossStagePlayer);
	safe_delete(bossStageEnemy);
	safe_delete(gameclearPlayer);
	safe_delete(gameoverPlayer);
}

void GameScene::Initialize(DirectXCommon* dxCommon, Input* input, Audio* audio)
{
	// nullptrチェック
	assert(dxCommon);
	assert(input);
	assert(audio);

	this->dxCommon = dxCommon;
	this->input = input;
	this->audio = audio;

	// テクスチャ読み込み
	Sprite::LoadTexture(debugTextTexNumber, L"Resources/debugfont.png");
	Sprite::LoadTexture(1, L"Resources/title.png");
	Sprite::LoadTexture(2, L"Resources/HP5.png");
	Sprite::LoadTexture(3, L"Resources/HP4.png");
	Sprite::LoadTexture(4, L"Resources/HP3.png");
	Sprite::LoadTexture(5, L"Resources/HP2.png");
	Sprite::LoadTexture(6, L"Resources/HP1.png");
	Sprite::LoadTexture(7, L"Resources/HP0.png");
	Sprite::LoadTexture(8, L"Resources/bossHP5.png");
	Sprite::LoadTexture(9, L"Resources/bossHP4.png");
	Sprite::LoadTexture(10, L"Resources/bossHP3.png");
	Sprite::LoadTexture(11, L"Resources/bossHP2.png");
	Sprite::LoadTexture(12, L"Resources/bossHP1.png");
	Sprite::LoadTexture(13, L"Resources/bossHP0.png");
	Sprite::LoadTexture(14, L"Resources/background1.png");
	Sprite::LoadTexture(15, L"Resources/background2.png");
	Sprite::LoadTexture(16, L"Resources/background3.png");
	Sprite::LoadTexture(17, L"Resources/menu1.png");
	Sprite::LoadTexture(18, L"Resources/menu2.png");
	Sprite::LoadTexture(19, L"Resources/menu3.png");
	Sprite::LoadTexture(20, L"Resources/sign1.png");
	Sprite::LoadTexture(21, L"Resources/sign2.png");
	Sprite::LoadTexture(22, L"Resources/sign3.png");
	Sprite::LoadTexture(23, L"Resources/sign4.png");
	Sprite::LoadTexture(24, L"Resources/sign5.png");
	Sprite::LoadTexture(25, L"Resources/sign6.png");
	Sprite::LoadTexture(26, L"Resources/sign7.png");
	Sprite::LoadTexture(27, L"Resources/gameclear.png");
	Sprite::LoadTexture(28, L"Resources/gameover1.png");
	Sprite::LoadTexture(29, L"Resources/gameover2.png");
	Sprite::LoadTexture(30, L"Resources/black.png");
	Sprite::LoadTexture(31, L"Resources/white.png");

	// スプライト生成
	debugText.Initialize(debugTextTexNumber);
	title = Sprite::Create(1, { 0.0f,0.0f }, { 1.25f,1.25f,1.25f,1 });
	HP5 = Sprite::Create(2, { 0.0f,0.0f }, { 2,2,2,1 });
	HP4 = Sprite::Create(3, { 0.0f,0.0f }, { 2,2,2,1 });
	HP3 = Sprite::Create(4, { 0.0f,0.0f }, { 2,2,2,1 });
	HP2 = Sprite::Create(5, { 0.0f,0.0f }, { 2,2,2,1 });
	HP1 = Sprite::Create(6, { 0.0f,0.0f }, { 2,2,2,1 });
	HP0 = Sprite::Create(7, { 0.0f,0.0f }, { 2,2,2,1 });
	bossHP5 = Sprite::Create(8, { 0.0f,0.0f }, { 2,2,2,1 });
	bossHP4 = Sprite::Create(9, { 0.0f,0.0f }, { 2,2,2,1 });
	bossHP3 = Sprite::Create(10, { 0.0f,0.0f }, { 2,2,2,1 });
	bossHP2 = Sprite::Create(11, { 0.0f,0.0f }, { 2,2,2,1 });
	bossHP1 = Sprite::Create(12, { 0.0f,0.0f }, { 2,2,2,1 });
	bossHP0 = Sprite::Create(13, { 0.0f,0.0f }, { 2,2,2,1 });
	background1 = Sprite::Create(14, { 0.0f,0.0f });
	background2 = Sprite::Create(15, { 0.0f,0.0f });
	background3 = Sprite::Create(16, { 0.0f,0.0f }, { 2,2,2,1 });
	menu1 = Sprite::Create(17, { 0.0f,0.0f }, { 2,2,2,1 });
	menu2 = Sprite::Create(18, { 0.0f,0.0f }, { 2,2,2,1 });
	menu3 = Sprite::Create(19, { 0.0f,0.0f }, { 2,2,2,1 });
	sign1 = Sprite::Create(20, { 0.0f,0.0f }, { 1.25f,1.25f,1.25f,1 });
	sign2 = Sprite::Create(21, { 0.0f,0.0f }, { 1.25f,1.25f,1.25f,1 });
	sign3 = Sprite::Create(22, { 0.0f,0.0f }, { 1.25f,1.25f,1.25f,1 });
	sign4 = Sprite::Create(23, { 0.0f,0.0f }, { 1.25f,1.25f,1.25f,1 });
	sign5 = Sprite::Create(24, { 0.0f,0.0f }, { 1.25f,1.25f,1.25f,1 });
	sign6 = Sprite::Create(25, { 0.0f,0.0f }, { 1.25f,1.25f,1.25f,1 });
	sign7 = Sprite::Create(26, { 0.0f,0.0f }, { 1.25f,1.25f,1.25f,1 });
	gameclear = Sprite::Create(27, { 0.0f,0.0f }, { 1.25f,1.25f,1.25f,1 });
	gameover1 = Sprite::Create(28, { 0.0f,0.0f }, { 2,2,2,1 });
	gameover2 = Sprite::Create(29, { 0.0f,0.0f }, { 2,2,2,1 });
	for (int i = 0; i < 6; i++) {
		black[i] = Sprite::Create(30, { -1380.0f, 0.0f });
	}
	black[6] = Sprite::Create(30, { 0.0f, 0.0f });
	white = Sprite::Create(31, { 0.0f,0.0f });

	// サウンド読み込み
	audio->SoundLoadWave("彩づく草原.wav");
	audio->SoundLoadWave("風のダンジョン.wav");
	audio->SoundLoadWave("風が吹く1.wav");
	audio->SoundLoadWave("ac_Athletic.wav");

	// タイトルプレイヤー生成
	titlePlayer = new TitlePlayer();
	titlePlayer->Initialize(input);
	// チュートリアルプレイヤー生成
	tutorialPlayer = new TutorialPlayer();
	tutorialPlayer->Initialize(input);
	// ステージ1プレイヤー生成
	stageOnePlayer = new StageOnePlayer();
	stageOnePlayer->Initialize(input);
	// ステージ1エネミー生成
	stageOneEnemy = new StageOneEnemy();
	stageOneEnemy->Initialize();
	// ステージ2プレイヤー生成
	stageTwoPlayer = new StageTwoPlayer();
	stageTwoPlayer->Initialize(input);
	// ステージ2エネミー生成
	stageTwoEnemy = new StageTwoEnemy();
	stageTwoEnemy->Initialize();
	// ステージ3プレイヤー生成
	stageThreePlayer = new StageThreePlayer();
	stageThreePlayer->Initialize(input);
	// ステージ3ハート生成
	stageThreeHeart = new StageThreeHeart();
	stageThreeHeart->Initialize();
	// ボスステージプレイヤー生成
	bossStagePlayer = new BossStagePlayer();
	bossStagePlayer->Initialize(input);
	// ボスステージエネミー生成
	bossStageEnemy = new BossStageEnemy();
	bossStageEnemy->Initialize();
	// ゲームクリアプレイヤー生成
	gameclearPlayer = new GameclearPlayer();
	gameclearPlayer->Initialize(input);
	// ゲームオーバープレイヤー生成
	gameoverPlayer = new GameoverPlayer();
	gameoverPlayer->Initialize(input);

	// カメラ生成
	camera = new DebugCamera(WinApp::window_width, WinApp::window_height, input);
	// 3Dオブジェクトにカメラをセット
	Object3d::SetCamera(camera);
	// カメラ注視点をセット
	camera->SetDistance(3.0f);
}

void GameScene::Update()
{
#ifdef _DEBUG
	if (input->TriggerKey(DIK_P)) { HP = 0; }
	if (input->TriggerKey(DIK_B)) { bossHP = 0; }
	if (input->TriggerKey(DIK_1)) { audio->SoundStop("彩づく草原.wav"); scene = 2; }
	if (input->TriggerKey(DIK_2)) { audio->SoundStop("彩づく草原.wav"); scene = 3; }
	if (input->TriggerKey(DIK_3)) { audio->SoundStop("彩づく草原.wav"); scene = 4; }
	if (input->TriggerKey(DIK_4)) { audio->SoundStop("彩づく草原.wav"); scene = 5; }
#endif
	if (scene == 0) {
		titlePlayer->Update();
	}
	if (scene == 1) {
		if (pauseFlag == 0) {
			tutorialPlayer->Update();
		}
	}
	if (scene == 2) {
		if (pauseFlag == 0) {
			stageOnePlayer->Update();
			stageOneEnemy->Update();
		}
	}
	if (scene == 3) {
		if (pauseFlag == 0) {
			stageTwoPlayer->Update();
			stageTwoEnemy->Update();
		}
	}
	if (scene == 4) {
		if (pauseFlag == 0) {
			stageThreePlayer->Update();
			stageThreeHeart->Update();
		}
	}
	if (scene == 5) {
		if (pauseFlag == 0) {
			bossStagePlayer->Update();
			bossStageEnemy->Update();
		}
	}
	if (scene == 6) {
		gameclearPlayer->Update();
	}
	if (scene == 7) {
		gameoverPlayer->Update();
	}
	// サウンド
	Sound();
	// 当たり判定
	Collision();
	// ポーズ
	Pause();
	// シーンチェンジ
	SceneChange();
	// カメラのセット
	SetCamera();
	// アップデート
	camera->Update();
}

void GameScene::Draw()
{
	// コマンドリストの取得
	ID3D12GraphicsCommandList* cmdList = dxCommon->GetCommandList();

#pragma region 背景スプライト描画
	// 背景スプライト描画前処理
	Sprite::PreDraw(cmdList);

	if (scene == 0) {
		background1->Draw();
		title->Draw();
	}
	if (scene == 1) { background1->Draw(); }
	if (scene == 2) { background2->Draw(); }
	if (scene == 3) { background2->Draw(); }
	if (scene == 4 || scene == 5) { background3->Draw(); }
	if (scene == 6) {
		background1->Draw();
		gameclear->Draw();
	}
	if (scene == 7) {
		if (endMenu == 0) {
			gameover1->Draw();
		}
		if (endMenu == 1) {
			gameover2->Draw();
		}
	}

	// スプライト描画後処理
	Sprite::PostDraw();
	// 深度バッファクリア
	dxCommon->ClearDepthBuffer();
#pragma endregion

#pragma region 3D描画
	// 3Dオブジェクトの描画
	Object3d::PreDraw(cmdList);

	if (scene == 0) {
		titlePlayer->Draw();
	}
	if (scene == 1) {
		tutorialPlayer->Draw();
	}
	if (scene == 2) {
		stageOnePlayer->Draw();
		stageOneEnemy->Draw();
	}
	if (scene == 3) {
		stageTwoPlayer->Draw();
		stageTwoEnemy->Draw();
	}
	if (scene == 4) {
		stageThreePlayer->Draw();
		stageThreeHeart->Draw();
	}
	if (scene == 5) {
		bossStagePlayer->Draw();
		bossStageEnemy->Draw();
	}
	if (scene == 6) {
		gameclearPlayer->Draw();
	}
	if (scene == 7) {
		gameoverPlayer->Draw();
	}

	Object3d::PostDraw();
#pragma endregion

#pragma region 前景スプライト描画
	// 前景スプライト描画前処理
	Sprite::PreDraw(cmdList);

	if (0 < scene && scene < 6) {
		if (HP == 5) { HP5->Draw(); }
		if (HP == 4) { HP4->Draw(); }
		if (HP == 3) { HP3->Draw(); }
		if (HP == 2) { HP2->Draw(); }
		if (HP == 1) { HP1->Draw(); }
		if (HP == 0) { HP0->Draw(); }
	}

	if (scene == 5) {
		if (bossHP == 5) { bossHP5->Draw(); }
		if (bossHP == 4) { bossHP4->Draw(); }
		if (bossHP == 3) { bossHP3->Draw(); }
		if (bossHP == 2) { bossHP2->Draw(); }
		if (bossHP == 1) { bossHP1->Draw(); }
		if (bossHP == 0) { bossHP0->Draw(); }
	}

	if (scene == 1) {
		if (signFlag[0] == 1) { sign1->Draw(); }
		if (signFlag[1] == 1) { sign2->Draw(); }
		if (signFlag[2] == 1) { sign3->Draw(); }
		if (signFlag[3] == 1) { sign4->Draw(); }
		if (signFlag[4] == 1) { sign5->Draw(); }
		if (signFlag[5] == 1) { sign6->Draw(); }
	}

	black[6]->Draw();
	white->Draw();

	if (pauseFlag == 1) {
		if (pauseMenu == 0) {
			menu1->Draw();
		}
		else if (pauseMenu == 1) {
			menu2->Draw();
		}
		else {
			menu3->Draw();
		}
	}

	if (signFlag[6] == 1) { sign7->Draw(); }

	for (int i = 0; i < 6; i++) {
		black[i]->Draw();
	}

	// デバッグテキストの描画
	debugText.DrawAll(cmdList);

	// スプライト描画後処理
	Sprite::PostDraw();
#pragma endregion
}

// サウンド
void GameScene::Sound()
{
	// 再生
	if (scene == 0) { audio->SoundPlayWave("彩づく草原.wav", true); }
	if (0 < scene && scene < 4) { audio->SoundPlayWave("風のダンジョン.wav", true); }
	if (scene == 4) { audio->SoundPlayWave("風が吹く1.wav", true); }
	if (scene == 5) { audio->SoundPlayWave("ac_Athletic.wav", true); }

	// 停止
	if (scene == 0) {
		audio->SoundStop("風のダンジョン.wav");
		audio->SoundStop("風が吹く1.wav");
		audio->SoundStop("ac_Athletic.wav");
	}
	if (scene == 1) {
		audio->SoundStop("彩づく草原.wav");
	}
	if (scene == 2 || scene == 3) {
		// 死んだら
		if (HP == 0) {
			audio->SoundStop("風のダンジョン.wav");
		}
	}
	if (scene == 4) {
		audio->SoundStop("風のダンジョン.wav");
	}
	if (scene == 5) {
		audio->SoundStop("風が吹く1.wav");
		// 死んだら
		if (HP == 0) {
			audio->SoundStop("ac_Athletic.wav");
		}
	}
	if (bossStageEnemy->GetEffectRange() < bossStageEnemy->GetEffectPositionY()) {
		audio->SoundStop("ac_Athletic.wav");
	}
}

// 当たり判定
void GameScene::Collision()
{
	if (scene == 1) {
		XMFLOAT3 PlayerPosition = tutorialPlayer->GetPosition();
		Input::StickMove stickMove = input->GetStickMove();
		// 看板を読む
		if ((input->TriggerKey(DIK_UP) || stickMove.lY == 0 && input->GetDevJoyStick()) && pauseFlag == 0 && freezeFlag == 0) {
			if (PlayerPosition.x < 2 && PlayerPosition.y == 0) {
				signFlag[0] = 1;
				freezeFlag = 1;
				tutorialPlayer->SetFreezeFlagTrue();
			}
			if (4 < PlayerPosition.x && PlayerPosition.x < 8 && PlayerPosition.y == 0) {
				signFlag[1] = 1;
				freezeFlag = 1;
				tutorialPlayer->SetFreezeFlagTrue();
			}
			if (25 < PlayerPosition.x && PlayerPosition.x < 29 && PlayerPosition.y == 0) {
				signFlag[2] = 1;
				freezeFlag = 1;
				tutorialPlayer->SetFreezeFlagTrue();
			}
			if (31 < PlayerPosition.x && PlayerPosition.x < 35 && PlayerPosition.y == 0) {
				signFlag[3] = 1;
				freezeFlag = 1;
				tutorialPlayer->SetFreezeFlagTrue();
			}
			if (43 < PlayerPosition.x && PlayerPosition.x < 47 && PlayerPosition.y == 12) {
				signFlag[4] = 1;
				freezeFlag = 1;
				tutorialPlayer->SetFreezeFlagTrue();
			}
			if (61 < PlayerPosition.x && PlayerPosition.x < 65 && PlayerPosition.y == 12) {
				signFlag[5] = 1;
				freezeFlag = 1;
				tutorialPlayer->SetFreezeFlagTrue();
			}
		}
		if (input->TriggerKey(DIK_X) || input->TriggerButton(1)) {
			if (PlayerPosition.x < 2 && PlayerPosition.y == 0) {
				signFlag[0] = 0;
				freezeFlag = 0;
				tutorialPlayer->SetFreezeFlagFalse();
			}
			if (4 < PlayerPosition.x && PlayerPosition.x < 8 && PlayerPosition.y == 0) {
				signFlag[1] = 0;
				freezeFlag = 0;
				tutorialPlayer->SetFreezeFlagFalse();
			}
			if (25 < PlayerPosition.x && PlayerPosition.x < 29 && PlayerPosition.y == 0) {
				signFlag[2] = 0;
				freezeFlag = 0;
				tutorialPlayer->SetFreezeFlagFalse();
			}
			if (31 < PlayerPosition.x && PlayerPosition.x < 35 && PlayerPosition.y == 0) {
				signFlag[3] = 0;
				freezeFlag = 0;
				tutorialPlayer->SetFreezeFlagFalse();
			}
			if (43 < PlayerPosition.x && PlayerPosition.x < 47 && PlayerPosition.y == 12) {
				signFlag[4] = 0;
				freezeFlag = 0;
				tutorialPlayer->SetFreezeFlagFalse();
			}
			if (61 < PlayerPosition.x && PlayerPosition.x < 65 && PlayerPosition.y == 12) {
				signFlag[5] = 0;
				freezeFlag = 0;
				tutorialPlayer->SetFreezeFlagFalse();
			}
		}
	}

	if (scene == 2) {
		XMFLOAT3 PlayerPosition = stageOnePlayer->GetPosition();
		XMFLOAT3 EnemyPosition[4] = { stageOneEnemy->GetPosition1(), stageOneEnemy->GetPosition2(),
			stageOneEnemy->GetPosition3(), stageOneEnemy->GetPosition4() };
		// エネミー1の当たり判定
		if (EnemyPosition[0].x - 2 < PlayerPosition.x && PlayerPosition.x < EnemyPosition[0].x + 2 &&
			PlayerPosition.y < 2 && stageOneEnemy->GetDeathFlag1() == 0 && 0 < HP) {
			if (stageOnePlayer->GetAttackFlag() == 0 && stageOnePlayer->GetDamageCount() == 0) {
				stageOnePlayer->SetDamageCount();
				HP--;
			}
			if (0 < stageOnePlayer->GetAttackFlag()) {
				stageOneEnemy->SetEffect1();
				stageOneEnemy->SetDeathFlag1();
			}
		}
		// エネミー2の当たり判定
		if (EnemyPosition[1].x - 2 < PlayerPosition.x && PlayerPosition.x < EnemyPosition[1].x + 2 &&
			PlayerPosition.y < 14 && stageOneEnemy->GetDeathFlag2() == 0 && 0 < HP) {
			if (stageOnePlayer->GetAttackFlag() == 0 && stageOnePlayer->GetDamageCount() == 0) {
				stageOnePlayer->SetDamageCount();
				HP--;
			}
			if (0 < stageOnePlayer->GetAttackFlag()) {
				stageOneEnemy->SetEffect2();
				stageOneEnemy->SetDeathFlag2();
			}
		}
		// エネミー3の当たり判定
		if (EnemyPosition[2].x - 2 < PlayerPosition.x && PlayerPosition.x < EnemyPosition[2].x + 2 &&
			PlayerPosition.y < 2 && stageOneEnemy->GetDeathFlag3() == 0 && 0 < HP) {
			if (stageOnePlayer->GetAttackFlag() < 2 && stageOnePlayer->GetDamageCount() == 0) {
				stageOnePlayer->SetDamageCount();
				HP--;
			}
			if (stageOnePlayer->GetAttackFlag() == 2) {
				stageOneEnemy->SetEffect3();
				stageOneEnemy->SetDeathFlag3();
			}
		}
		// エネミー4の当たり判定
		if (EnemyPosition[3].x - 2 < PlayerPosition.x && PlayerPosition.x < EnemyPosition[3].x + 2 &&
			PlayerPosition.y < 2 && stageOneEnemy->GetDeathFlag4() == 0 && 0 < HP) {
			if (stageOnePlayer->GetAttackFlag() < 2 && stageOnePlayer->GetDamageCount() == 0) {
				stageOnePlayer->SetDamageCount();
				HP--;
			}
			if (stageOnePlayer->GetAttackFlag() == 2) {
				stageOneEnemy->SetEffect4();
				stageOneEnemy->SetDeathFlag4();
			}
		}
	}

	if (scene == 3) {
		XMFLOAT3 PlayerPosition = stageTwoPlayer->GetPosition();
		XMFLOAT3 EnemyPosition[3] = { stageTwoEnemy->GetPosition1(), stageTwoEnemy->GetPosition2(), stageTwoEnemy->GetPosition3() };
		// エネミー1の当たり判定
		if (EnemyPosition[0].x - 2 < PlayerPosition.x && PlayerPosition.x < EnemyPosition[0].x + 2 &&
			PlayerPosition.y < 2 && stageTwoEnemy->GetDeathFlag1() == 0 && 0 < HP) {
			if (stageTwoPlayer->GetAttackFlag() == 0 && stageTwoPlayer->GetDamageCount() == 0) {
				stageTwoPlayer->SetDamageCount();
				HP--;
			}
			if (0 < stageTwoPlayer->GetAttackFlag()) {
				stageTwoEnemy->SetEffect1();
				stageTwoEnemy->SetDeathFlag1();
			}
		}
		// エネミー2の当たり判定
		if (EnemyPosition[1].x - 2 < PlayerPosition.x && PlayerPosition.x < EnemyPosition[1].x + 2 &&
			PlayerPosition.y < 2 && stageTwoEnemy->GetDeathFlag2() == 0 && 0 < HP) {
			if (stageTwoPlayer->GetAttackFlag() == 0 && stageTwoPlayer->GetDamageCount() == 0) {
				stageTwoPlayer->SetDamageCount();
				HP--;
			}
			if (0 < stageTwoPlayer->GetAttackFlag()) {
				stageTwoEnemy->SetEffect2();
				stageTwoEnemy->SetDeathFlag2();
			}
		}
		// エネミー3の当たり判定
		if (EnemyPosition[2].x - 2 < PlayerPosition.x && PlayerPosition.x < EnemyPosition[2].x + 2 &&
			PlayerPosition.y < 2 && stageTwoEnemy->GetDeathFlag3() == 0 && 0 < HP) {
			if (stageTwoPlayer->GetAttackFlag() < 2 && stageTwoPlayer->GetDamageCount() == 0) {
				stageTwoPlayer->SetDamageCount();
				HP--;
			}
			if (stageTwoPlayer->GetAttackFlag() == 2) {
				stageTwoEnemy->SetEffect3();
				stageTwoEnemy->SetDeathFlag3();
			}
		}
	}

	if (scene == 4) {
		XMFLOAT3 PlayerPosition = stageThreePlayer->GetPosition();
		// HPを回復する
		if (7 < PlayerPosition.x && PlayerPosition.x < 11 && PlayerPosition.y < 3 && stageThreeHeart->GetHeartFlag() == 0) {
			HP = 5;
			stageThreeHeart->SetHeartFlag();
		}
	}

	if (scene == 5) {
		XMFLOAT3 PlayerPosition = bossStagePlayer->GetPosition();
		XMFLOAT3 EnemyPosition = bossStageEnemy->GetPosition();
		// ボスの当たり判定
		if (EnemyPosition.x - 3 < PlayerPosition.x && PlayerPosition.x < EnemyPosition.x + 3 &&
			EnemyPosition.y - 3 < PlayerPosition.y && PlayerPosition.y < EnemyPosition.y + 3 &&
			EnemyPosition.z - 3 < PlayerPosition.z && PlayerPosition.z < EnemyPosition.z + 3 &&
			bossStageEnemy->GetDamageCount() == 0 && 0 < HP && 0 < bossHP) {
			// ボスが第一形態の時
			if (bossStagePlayer->GetAttackFlag() == 0 && bossStagePlayer->GetDamageCount() == 0 &&
				bossStageEnemy->GetTransformFlag() == 0) {
				bossStagePlayer->SetDamageCount();
				HP--;
			}
			if (0 < bossStagePlayer->GetAttackFlag() && bossStageEnemy->GetTransformFlag() == 0) {
				bossStageEnemy->SetDamageCount();
				bossHP--;
			}
			// ボスが第ニ、最終形態の時
			if (bossStagePlayer->GetAttackFlag() < 2 && bossStagePlayer->GetDamageCount() == 0 &&
				0 < bossStageEnemy->GetTransformFlag()) {
				bossStagePlayer->SetDamageCount();
				HP--;
			}
			if (bossStagePlayer->GetAttackFlag() == 2 && 0 < bossStageEnemy->GetTransformFlag()) {
				bossStageEnemy->SetDamageCount();
				bossHP--;
			}
		}
		// ボスのHPが4になったら
		if (bossHP == 4) {
			bossStageEnemy->SetJumpHeight1();
		}
		// ボスのHPが3になったら
		if (bossHP == 3) {
			bossStageEnemy->SetSpeed1();
			bossStageEnemy->SetTransformFlag1();
		}
		// ボスのHPが2になったら
		if (bossHP == 2) {
			bossStageEnemy->SetJumpHeight2();
		}
		// ボスのHPが1になったら
		if (bossHP == 1) {
			if (bossStageEnemy->GetTransformFlag() == 1) {
				bossStageEnemy->SetJumpPower();
				bossStageEnemy->SetTransformFlag2();
			}
		}
	}
}

// ポーズ
void GameScene::Pause()
{
	Input::StickMove stickMove = input->GetStickMove();
	// ポーズ、ポーズ解除
	if (0 < scene && scene < 6) {
		if ((input->TriggerKey(DIK_ESCAPE) || input->TriggerButton(7)) && 0 < HP && 0 < bossHP) {
			if (pauseFlag == 0 && freezeFlag == 0) {
				pauseFlag = 1;
				BlackColor.w = 0.5f;
			}
			else if (pauseFlag == 1 && signFlag[6] == 0) {
				pauseMenu = 0;
				pauseFlag = 0;
				freezeFlag = 0;
				BlackColor.w = 0.0f;
			}
		}
	}
	// ポーズ中
	if (pauseFlag == 1) {
		// メニュー移動
		if ((input->TriggerKey(DIK_LEFT) || stickMove.lX == 0 && input->GetDevJoyStick() && freezeFlag == 0) && signFlag[6] == 0) {
			if (pauseMenu == 0) {
				freezeFlag = 1;
				pauseMenu = 1;
			}
			else if (pauseMenu == 2) {
				freezeFlag = 1;
				pauseMenu = 0;
			}
		}
		if ((input->TriggerKey(DIK_RIGHT) || stickMove.lX == 65535 && input->GetDevJoyStick() && freezeFlag == 0) && signFlag[6] == 0) {
			if (pauseMenu == 0) {
				freezeFlag = 1;
				pauseMenu = 2;
			}
			else if (pauseMenu == 1) {
				freezeFlag = 1;
				pauseMenu = 0;
			}
		}
		if ((0 < stickMove.lX && stickMove.lX < 65535) && input->GetDevJoyStick()) {
			freezeFlag = 0;
		}

		// メニュー決定
		if (input->TriggerKey(DIK_Z) || input->TriggerButton(0)) {
			if (pauseMenu == 0) {
				pauseFlag = 0;
				freezeFlag = 0;
				BlackColor.w = 0.0f;
			}
			if (pauseMenu == 1) {
				signFlag[6] = 1;
			}
			if (pauseMenu == 2) {
				titlePlayer->Reset();
				sceneChangeFlag[5] = 1;
			}
		}

		// アクション表示解除
		if ((input->TriggerKey(DIK_X) || input->TriggerButton(1)) && signFlag[6] == 1) {
			signFlag[6] = 0;
		}
	}
}

// シーンチェンジ
void GameScene::SceneChange()
{
	if (scene == 0) {
		if ((input->TriggerKey(DIK_Z) || input->TriggerButton(0)) && freezeFlag == 0) {
			Reset();
			sceneChangeFlag[0] = 1;
		}
	}

	if (scene == 1) {
		XMFLOAT3 PlayerPosition = tutorialPlayer->GetPosition();
		Input::StickMove stickMove = input->GetStickMove();
		if ((input->TriggerKey(DIK_UP) || stickMove.lY == 0 && input->GetDevJoyStick()) && pauseFlag == 0 &&
			88 < PlayerPosition.x && PlayerPosition.x < 92 && PlayerPosition.y == 0) {
			sceneChangeFlag[1] = 1;
		}
	}

	if (scene == 2) {
		XMFLOAT3 PlayerPosition = stageOnePlayer->GetPosition();
		Input::StickMove stickMove = input->GetStickMove();
		if ((input->TriggerKey(DIK_UP) || stickMove.lY == 0 && input->GetDevJoyStick()) && pauseFlag == 0 &&
			88 < PlayerPosition.x && PlayerPosition.x < 92 && PlayerPosition.y == 0) {
			sceneChangeFlag[2] = 1;
		}
		// 死んだら
		if (HP == 0) {
			stageOnePlayer->SetDeathFlag();
		}
		if (stageOnePlayer->GetDeathFlag() == 1 && stageOnePlayer->GetDamageCount() == 1) {
			stageOnePlayer->SetEffect();
		}
		if (stageOnePlayer->GetEffectRange() < stageOnePlayer->GetEffectPositionY()) {
			BlackColor.w += 0.02f;
		}
		if (1 < BlackColor.w) {
			scene = 7;
		}
	}

	if (scene == 3) {
		XMFLOAT3 PlayerPosition = stageTwoPlayer->GetPosition();
		Input::StickMove stickMove = input->GetStickMove();
		if ((input->TriggerKey(DIK_UP) || stickMove.lY == 0 && input->GetDevJoyStick()) && pauseFlag == 0 &&
			88 < PlayerPosition.x && PlayerPosition.x < 92 && PlayerPosition.y == 0) {
			sceneChangeFlag[3] = 1;
		}
		// 死んだら
		if (HP == 0) {
			stageTwoPlayer->SetDeathFlag();
		}
		if (stageTwoPlayer->GetDeathFlag() == 1 && stageTwoPlayer->GetDamageCount() == 1) {
			stageTwoPlayer->SetEffect();
		}
		if (stageTwoPlayer->GetEffectRange() < stageTwoPlayer->GetEffectPositionY()) {
			BlackColor.w += 0.02f;
		}
		if (1 < BlackColor.w) {
			scene = 7;
		}
	}

	if (scene == 4) {
		XMFLOAT3 PlayerPosition = stageThreePlayer->GetPosition();
		Input::StickMove stickMove = input->GetStickMove();
		if ((input->TriggerKey(DIK_UP) || stickMove.lY == 0 && input->GetDevJoyStick()) && pauseFlag == 0 &&
			37 < PlayerPosition.x && PlayerPosition.x < 41 && PlayerPosition.y == 0) {
			sceneChangeFlag[4] = 1;
		}
	}

	if (scene == 5) {
		// ボスを倒したら
		if (bossHP == 0) {
			bossStageEnemy->SetDeathFlag();
		}
		if (bossStageEnemy->GetDeathFlag() == 1 && bossStageEnemy->GetDamageCount() == 1) {
			bossStageEnemy->SetEffect();
		}
		if (bossStageEnemy->GetEffectRange() < bossStageEnemy->GetEffectPositionY()) {
			WhiteColor.w += 0.02f;
		}
		if (1 < WhiteColor.w) {
			scene = 6;
		}
		// 死んだら
		if (HP == 0) {
			bossStagePlayer->SetDeathFlag();
		}
		if (bossStagePlayer->GetDeathFlag() == 1 && bossStagePlayer->GetDamageCount() == 1) {
			bossStagePlayer->SetEffect();
		}
		if (bossStagePlayer->GetEffectRange() < bossStagePlayer->GetEffectPositionY()) {
			BlackColor.w += 0.02f;
		}
		if (1 < BlackColor.w) {
			scene = 7;
		}
	}

	if (scene == 6) {
		if ((input->TriggerKey(DIK_Z) || input->TriggerButton(0)) && WhiteColor.w < 0) {
			titlePlayer->Reset();
			sceneChangeFlag[5] = 1;
		}
	}

	if (scene == 7) {
		Input::StickMove stickMove = input->GetStickMove();
		if ((input->TriggerKey(DIK_RIGHT) || stickMove.lX == 65535 && input->GetDevJoyStick()) &&
			BlackColor.w < 0 && endMenu == 0 && freezeFlag == 0) {
			endMenu = 1;
		}
		if ((input->TriggerKey(DIK_LEFT) || stickMove.lX == 0 && input->GetDevJoyStick()) &&
			BlackColor.w < 0 && endMenu == 1 && freezeFlag == 0) {
			endMenu = 0;
		}
		if (input->TriggerKey(DIK_Z) || input->TriggerButton(0) && BlackColor.w < 0 && freezeFlag == 0) {
			if (stageOnePlayer->GetDeathFlag() == 1 && endMenu == 0) {
				Reset();
				gameoverPlayer->SetContinueFlagTrue();
				sceneChangeFlag[1] = 1;
			}
			else if (stageTwoPlayer->GetDeathFlag() == 1 && endMenu == 0) {
				Reset();
				gameoverPlayer->SetContinueFlagTrue();
				sceneChangeFlag[2] = 1;
			}
			else if (bossStagePlayer->GetDeathFlag() == 1 && endMenu == 0) {
				Reset();
				gameoverPlayer->SetContinueFlagTrue();
				sceneChangeFlag[4] = 1;
			}
			else {
				titlePlayer->Reset();
				sceneChangeFlag[5] = 1;
			}
		}
	}

	// シーン推移
	if (BlackPosition[0].x == -100) { scene = 1; }
	if (BlackPosition[1].x == -100) { scene = 2; }
	if (BlackPosition[2].x == -100) { scene = 3; }
	if (BlackPosition[3].x == -100) { scene = 4; }
	if (BlackPosition[4].x == -100) { scene = 5; }
	if (BlackPosition[5].x == -100) {
		scene = 0;
		pauseFlag = 0;
		BlackColor.w = 0.0f;
	}

	if (sceneChangeFlag[0] == 1) {
		if (BlackPosition[0].x < 1380) {
			freezeFlag = 1;
			tutorialPlayer->SetFreezeFlagTrue();
			BlackPosition[0].x += 20.0f;
		}
		else {
			freezeFlag = 0;
			tutorialPlayer->SetFreezeFlagFalse();
			sceneChangeFlag[0] = 0;
		}
	}

	if (sceneChangeFlag[1] == 1) {
		if (BlackPosition[1].x < 1380) {
			freezeFlag = 1;
			tutorialPlayer->SetFreezeFlagTrue();
			stageOnePlayer->SetFreezeFlagTrue();
			stageOneEnemy->SetFreezeFlagTrue();
			BlackPosition[1].x += 20.0f;
		}
		else {
			freezeFlag = 0;
			stageOnePlayer->SetFreezeFlagFalse();
			stageOneEnemy->SetFreezeFlagFalse();
			sceneChangeFlag[1] = 0;
		}
	}

	if (sceneChangeFlag[2] == 1) {
		if (BlackPosition[2].x < 1380) {
			freezeFlag = 1;
			stageOnePlayer->SetFreezeFlagTrue();
			stageOneEnemy->SetFreezeFlagTrue();
			stageTwoPlayer->SetFreezeFlagTrue();
			stageTwoEnemy->SetFreezeFlagTrue();
			BlackPosition[2].x += 20.0f;
		}
		else {
			freezeFlag = 0;
			stageTwoPlayer->SetFreezeFlagFalse();
			stageTwoEnemy->SetFreezeFlagFalse();
			sceneChangeFlag[2] = 0;
		}
	}

	if (sceneChangeFlag[3] == 1) {
		if (BlackPosition[3].x < 1380) {
			freezeFlag = 1;
			stageTwoPlayer->SetFreezeFlagTrue();
			stageTwoEnemy->SetFreezeFlagTrue();
			stageThreePlayer->SetFreezeFlagTrue();
			stageThreeHeart->SetFreezeFlagTrue();
			BlackPosition[3].x += 20.0f;
		}
		else {
			freezeFlag = 0;
			stageThreePlayer->SetFreezeFlagFalse();
			stageThreeHeart->SetFreezeFlagFalse();
			sceneChangeFlag[3] = 0;
		}
	}

	if (sceneChangeFlag[4] == 1) {
		if (BlackPosition[4].x < 1380) {
			freezeFlag = 1;
			stageThreePlayer->SetFreezeFlagTrue();
			stageThreeHeart->SetFreezeFlagTrue();
			bossStagePlayer->SetFreezeFlagTrue();
			bossStageEnemy->SetFreezeFlagTrue();
			BlackPosition[4].x += 20.0f;
		}
		else {
			freezeFlag = 0;
			bossStagePlayer->SetFreezeFlagFalse();
			bossStageEnemy->SetFreezeFlagFalse();
			sceneChangeFlag[4] = 0;
		}
	}

	if (sceneChangeFlag[5] == 1) {
		if (BlackPosition[5].x < 1380) {
			freezeFlag = 1;
			BlackPosition[5].x += 20.0f;
		}
		else {
			freezeFlag = 0;
			sceneChangeFlag[5] = 0;
		}
	}

	if (scene == 6 && 0 < WhiteColor.w) {
		WhiteColor.w -= 0.02f;
	}

	if (scene == 7 && 0 < BlackColor.w) {
		gameoverPlayer->Reset();
		BlackColor.w -= 0.02f;
	}

	// セットポジション
	for (int i = 0; i < 6; i++) {
		black[i]->SetPosition(BlackPosition[i]);
	}

	// セットカラー
	black[6]->SetColor(BlackColor);
	white->SetColor(WhiteColor);
}

// カメラのセット
void GameScene::SetCamera()
{
	if (scene == 0) {
		XMFLOAT3 CameraPosition = titlePlayer->GetTarget();
		camera->SetTarget(CameraPosition);
	}
	if (scene == 1) {
		XMFLOAT3 CameraPosition = tutorialPlayer->GetTarget();
		camera->SetTarget(CameraPosition);
	}
	if (scene == 2) {
		XMFLOAT3 CameraPosition = stageOnePlayer->GetTarget();
		camera->SetTarget(CameraPosition);
	}
	if (scene == 3) {
		XMFLOAT3 CameraPosition = stageTwoPlayer->GetTarget();
		camera->SetTarget(CameraPosition);
	}
	if (scene == 4) {
		XMFLOAT3 CameraPosition = stageThreePlayer->GetTarget();
		camera->SetTarget(CameraPosition);
	}
	if (scene == 5) {
		XMFLOAT3 CameraPosition = bossStagePlayer->GetTarget();
		camera->SetTarget(CameraPosition);
	}
	if (scene == 6) {
		XMFLOAT3 CameraPosition = gameclearPlayer->GetTarget();
		camera->SetTarget(CameraPosition);
	}
	if (scene == 7) {
		XMFLOAT3 CameraPosition = gameoverPlayer->GetTarget();
		camera->SetTarget(CameraPosition);
	}
}

// リセット
void GameScene::Reset()
{
	HP = 5;
	bossHP = 5;
	pauseMenu = 0;
	endMenu = 0;
	for (int i = 0; i < 6; i++) {
		sceneChangeFlag[i] = 0;
	}
	for (int i = 0; i < 7; i++) {
		if (i < 6) {
			BlackPosition[i] = { -1380, 0 };
		}
		if (i == 6) {
			BlackPosition[i] = { 0, 0 };
		}
	}
	BlackColor = { 1,1,1,0 };
	WhiteColor = { 1,1,1,0 };
	tutorialPlayer->Reset();
	stageOnePlayer->Reset();
	stageOneEnemy->Reset();
	stageTwoPlayer->Reset();
	stageTwoEnemy->Reset();
	stageThreePlayer->Reset();
	stageThreeHeart->Reset();
	bossStagePlayer->Reset();
	bossStageEnemy->Reset();
	gameclearPlayer->Reset();
}