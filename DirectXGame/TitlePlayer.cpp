#include "TitlePlayer.h"

// コンストラクタ
TitlePlayer::TitlePlayer()
{
}

// デストラクタ
TitlePlayer::~TitlePlayer()
{
	safe_delete(modelPlayerRight);
	safe_delete(objPlayerRight);
	safe_delete(simpleMap);
}

// 初期化
void TitlePlayer::Initialize(Input* input)
{
	this->input = input;

	// カメラ生成
	camera = new DebugCamera(WinApp::window_width, WinApp::window_height, input);

	// 3Dオブジェクトにカメラをセット
	Object3d::SetCamera(camera);

	// カメラ注視点をセット
	camera->SetDistance(3.0f);

	// モデル読み込み
	modelPlayerRight = Model::CreateFromOBJ("playerRight");

	// 3Dオブジェクト生成
	objPlayerRight = Object3d::Create(modelPlayerRight);

	// シンプルマップ生成
	simpleMap = new SimpleMap();
	simpleMap->Initialize();
}

// 更新
void TitlePlayer::Update()
{
	// ゲッター
	Getter();

	// アクション
	Action();

	// セッター
	Setter();

	// アップデート
	camera->Update();

	objPlayerRight->Update();
	simpleMap->Update();
}

// 描画
void TitlePlayer::Draw()
{
	objPlayerRight->Draw();
	simpleMap->Draw();
}

// ゲッター
void TitlePlayer::Getter()
{
	// ゲットターゲット
	camera->GetTarget();

	// ゲットポジション
	objPlayerRight->GetPosition();

	// ゲットローテーション
	objPlayerRight->GetRotation();
}

// セッター
void TitlePlayer::Setter()
{
	// セットターゲット
	camera->SetTarget(CameraPosition);

	// セットポジション
	objPlayerRight->SetPosition(PlayerPosition);

	// セットローテーション
	objPlayerRight->SetRotation(PlayerRotation);
}

// アクション
void TitlePlayer::Action()
{
	// 移動
	PlayerPosition.x += 0.2f;
	if (48 < PlayerPosition.x) {
		PlayerPosition.x = -6.0f;
	}

	// 回転
	PlayerRotation.z -= 3.6f;
	if (PlayerRotation.z == -360) {
		PlayerRotation.z = 0.0f;
	}
}

// リセット
void TitlePlayer::Reset()
{
	CameraPosition = { 21, 8.3f, -20 };
	PlayerPosition = { 0,0,0 };
	PlayerRotation = { 0,0,0 };
}