#pragma once

#include "Object3d.h"
#include "SafeDelete.h"

using namespace DirectX;

class StageThreeHeart
{
public: // メンバ関数
	// コンストラクタ
	StageThreeHeart();

	// デストラクタ
	~StageThreeHeart();

	// 初期化
	void Initialize();

	// 更新
	void Update();

	// 描画
	void Draw();

	// ゲッター
	void Getter();

	// セッター
	void Setter();

	// アクション
	void Action();

	// エフェクト
	void Effect();

	// リセット
	void Reset();

	// ハートフラグの取得
	bool GetHeartFlag() { return heartFlag; }

	// フリーズフラグの設定(True)
	void SetFreezeFlagTrue() { freezeFlag = 1; }

	// フリーズフラグの設定(False)
	void SetFreezeFlagFalse() { freezeFlag = 0; }

	// ハートフラグの設定
	void SetHeartFlag() { heartFlag = 1; }

private: // メンバ変数
	bool freezeFlag = 0;
	bool heartFlag = 0;

	XMFLOAT3 HeartPosition = { 9,0,0 };
	XMFLOAT3 HeartRotation = { 0,0,0 };

	XMFLOAT3 EffectPosition[8] = {};
	float effectRange = 4.5f;

	Model* modelHeart = nullptr;
	Model* modelRhombus = nullptr;

	Object3d* objHeart = nullptr;
	Object3d* objEffect[8] = {};
};